function ReadBigFile()

    path = 'C:\Users\Mats\Desktop\test.wav';
    tic
    [audio, SR] = audioread(path);
    toc
    tic
    audio = Descriptors.testMono(audio);
    toc
    idx = 8*SR;
    disp(length(audio)/(44100*8));
    onesnare = audio(55*idx+1:56*idx);
    sound(onesnare,SR);

end