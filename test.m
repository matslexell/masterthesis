function v = test(file)


%----- USER DATA -----
[DAFx_in, SR]  = wavread(file);
hop            = 256;  % hop size between two FFTs
WindowLength   = 1024; % length of the windows
w              = hanningz(WindowLength);

%----- some initialisations -----
WindowLength2  = WindowLength/2;
normW          = norm(w,2);
pft            = 1;
lf             = floor((length(DAFx_in) - WindowLength)/hop);
feature_rms    = zeros(lf,1);


tic
%===========================================
pin  = 0;
pend = length(DAFx_in) - WindowLength;

while pin<pend
   grain = DAFx_in(pin+1:pin+WindowLength).* w;
   feature_rms(pft) = norm(grain,2) / normW;
   pft   = pft + 1;
   pin   = pin + hop;
end
% ===========================================
toc

n_signal = 1:pend; 
t_signal = (n_signal-1)/SR; % time in sec

t_rms = (0:pft-2)*hop/SR;

subplot(2,2,1); 
plot(t_signal, DAFx_in(n_signal)); 
title('Signal')
xlabel('t/s \rightarrow')
%axis([0 max(t_signal) [-1.2 1.2]*max(abs(DAFx_in))])
axis([0 max(t_signal) -1 1])

subplot(2,2,2); 
plot(t_rms, feature_rms);
title('RMS')
xlabel('t/s \rightarrow')
%axis([0 max(t_signal) 0 1.2*max(feature_rms)])
axis([0 max(t_signal) 0 1]);


% SOME TESTS %
v = [];


v(1) = 1213;

v(4) = 3;
disp('lol');
end