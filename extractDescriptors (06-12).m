% M-file 9.24
% UX_rms.m

function extractDescriptors()
file = 'crash.wav';


[audio, SR] = audioread(file);
%audio = audio(ceil(length(audio)/2):length(audio));
%audio = whitenoise();
%audio = [audio' audio']';
%audio = sinWav(440,20)';



%disp(attackLength(from, to , SR));

[atcFrom, atcTo] = attackIndex(audio);
dcyFrom = atcTo;
dcyTo = length(audio);

%ATTACK DESCRIPTORS
[sFrom,sTo] = attackTime(atcFrom, atcTo, SR);
frameSize = 1024;
hop = frameSize/2;

%
% #1 ATTACK ENERGY
atc_e = energy(audio(atcFrom:atcTo));

% #2 TEMPORAL CENTROID
atc_tc = temporalCentroid(audio(atcFrom:atcTo),SR,frameSize, hop);

% #3  LOG ATTACK-TIME
atc_lat = logAttackTime(atcFrom, atcTo, SR);

% #4  ATTACK ZERO-CROSSING RATE
atc_zcr = zeroCrossingRate(audio(atcFrom:atcTo));

% #5  TC/EA
atc_tcea = TC_EA(atcFrom, atcTo, SR, atc_tc);


%DECAY DESCRIPTORS
frameSize = 1024*32;  %//TODO
hop = frameSize/2;
sa = spectralAnalysis(audio(dcyFrom:dcyTo), SR, frameSize, hop);
%saVector = spectralAnalysisVector(audio,SR);


% #6  DECAY SPECTRAL FLATNESS
dcy_sf = spectralFlatness(sa);

% #7  DECAY SPECTRAL CENTROID
%centroid_first = spectralCentroid(sa);
[dcy_sc, dcy_scv] = spectralCentroid(sa);


% #8  DECAY STRONG PEAK
dcy_sp = strongPeak(toVec(sa));

% #9  DECAY SPECTRAL KURTOSIS
%sk = spectralKurtosis_old(saVector, sc);
dcy_sk = spectralKurtosis(sa, dcy_sc);

% #10 DECAY ZERO-CROSSING RATE
%dcy_zcr = zeroCrossingRate(audio);
[dcy_zcr, dcy_zcrVar] = zeroCrossingRateVar(audio(dcyFrom:dcyTo,frameSize);

% #11 STRONG DECAY
dcy_sd = strongDecay(audio(dcyFrom:dcyTo, SR);

% #12 DECAY SPECTRAL CENTROID VARIANCE
%TAKE dcy_scv from #7!

% #13 DECAY ZERO-CROSSING RATE VARIANCE
%TAKE dcy_zcrVar from #10!

% #14 DECAY SKEWNESS
%dcy_ss = spectralSkewness_old(sa,dcy_sc);
dcy_ss = spectralSkewness(sa,dcy_sc);

% #15 - #22 
aeb = allEnergyBands(audio, SR, 1024*4, 512*4);


% Mel-Frequency Cepstrum Coefficients (mean values) %
% means #23 - #35, vars #36 - #48
[means, vars] = getMfcc(audio, SR);

features = [atc_e atc_tc atc_lat atc_zcr atc_tcea ...
    dcy_sf dcy_sc dcy_sp dcy_sk dcy_zcr dcy_sd dcy_scv ...
    dcy_zcrVar dcy_ss aeb means vars];

disp(features'); 

end

function aeb = allEnergyBands(audio, SR, frameSize, hop)

sa = spectralAnalysis(audio, SR, frameSize, hop);
w = hanningz(frameSize);

eb1 = energyBands(sa,w, frameSize, 40, 70);
eb2 = energyBands(sa,w, frameSize, 70, 110);
eb3 = energyBands(sa,w, frameSize, 130, 145);
eb4 = energyBands(sa,w, frameSize, 160, 190);
eb5 = energyBands(sa,w, frameSize, 300, 400);
eb6 = energyBands(sa,w, frameSize, 5000, 7000);
eb7 = energyBands(sa,w, frameSize, 7000, 10000);
eb8 = energyBands(sa,w, frameSize, 10000, 15000);

aeb = [eb1 eb2 eb3 eb4 eb5 eb6 eb7 eb8];

aeb = aeb / sum(aeb);

end

function spectrum = spectralAnalysis(audio, SR, frameSize, hop)

N = length(audio);
%frameSize = 1024*2;
WindowLength2  = floor(frameSize/2);
w = hanningz(frameSize);
%hop = 512;
spectrum = zeros(floor(N/hop),WindowLength2);

idx = 1;
pin  = 0;


while pin < N - frameSize
    
    grain = audio(pin+1:pin+frameSize);
    spectrum(idx, :) = spectAnSub(grain, w);
    pin = pin + hop;
    idx = idx + 1;
end

%Trim vector
spectrum = spectrum(1:idx-1, :);

%{
%PLOT VECTOR
Y = 20 * log10(spectrum);
millis = floor(1 * length(audio)/SR);
imagesc([0 millis],[0 SR/2],Y'); axis xy; colorbar;
%}

%Y = vector;
%Y = sumTimeVector(vector);
%Y = 20 * log10(Y);

end

function Y = toVec(matrix)

[r,c] = size(matrix);

Y = zeros(1,c);
for i = 1:c
    Y(i) = energy(matrix(1:r,i));
end



end

%Calculates the rms between the given idxs. No windowing needed.
function e = energy(audio)
%Length of vector could be: length(audio(from:to)),
%but it is also to - from + 1
e = norm(audio,2) / sqrt(length(audio));

end

function [attackBegin, attackEnd]  = attackIndex(audio)

peak = max(abs(audio));
attackBegin = find(abs(audio) >= peak*0.2,1,'first');
attackEnd = find(abs(audio) >= peak*0.8,1,'first');

end

function TC = temporalCentroid(audio,SR, WindowLength, hop)

DAFx_in = audio;

w              = hanningz(WindowLength);


%----- some initialisations -----
WindowLength2  = WindowLength/2;
normW          = norm(w,2);
pft            = 1;
lf             = floor((length(DAFx_in) - WindowLength)/hop);
feature_rms    = zeros(lf,1);


%tic
%===========================================
pin  = 0;
pend = length(DAFx_in) - WindowLength;

while pin<pend
    % A grain is a windowed part of the input signal
    grain = DAFx_in(pin+1:pin+WindowLength).* w;
    feature_rms(pft) = norm(grain,2) / normW;
    pft   = pft + 1;
    
    
    pin   = pin + hop;
end
% ===========================================
%toc

t_rms = (0:pft-2)*hop/SR;
TC = sum(feature_rms.*(t_rms')) / sum(feature_rms);


end

function l = sec(audio, SR)
l = length(audio)/SR;
end

function l = attackLength(from,to,SR)
l = (to-from)/SR;
end

function [sFrom,sTo] = attackTime(from, to, SR)
sFrom = from/SR;
sTo = to/SR;
end

function lat = logAttackTime(from, to, SR)
lat = log((to - from) / SR);
end

function zc = zeroCrossingRate(audio)

zc = 0;

for i = 2:length(audio)
    if(audio(i) * audio(i-1) < 0)
        zc = zc + 1;
    end
end

end

function [zc, zc_var] = zeroCrossingRateVar(audio, frameSize)

N = length(audio);
zCrossings = zeros(floor(N/frameSize), 1);

idx = 1;
pin = 0;

while pin < N - frameSize
grain = audio(pin+1:pin+frameSize);
zCrossings(idx) = zeroCrossingRate(grain);

pin = pin + frameSize-1;
idx = idx + 1;
end

%zCrossings(idx) = zeroCrossingRate(audio(pin:N));
%zCrossings = zCrossings(1:idx-1);

zc_var = var(zCrossings);
zc = sum(zCrossings);

end

function tc_ea = TC_EA( from, to, SR, TC)
attackLength = (to-from)/SR;
tc_ea = TC/attackLength;
end

function sa = spectralAnalysisVector(audio, SR)

%use WGN for creating white noise
%audio = wgn(length(audio),1,100);
N=length(audio);
w = hanningz(N);

f = SR/N .* (0:N-1);
f = f(1:N);

%Use a hanningz to avoid spectral leakage
audio = audio .* w;

%This is the normalizing facotor
norm = N / sum(w);

% transformation -> returns complex coefficients
Y = fft(audio, N);
% compute abs for plot and normalize to N
% Normalize to 0.5N instead... Because Nyquist!
Y = abs( Y(1:N) ) ./ (0.5*N);

%Normalize to compensate for hanningz window
Y = Y .* norm; %-9.0352
Y(Y==0) = eps; % avoiding log(0) --> >> help eps
limit = round(length(audio) * (22050/SR));



%{
%--- CODE FOR PLOTTING
subplot(2,2,1);
plotY = Y; plotY = 20 * log10(Y); % turn spec logarithmic
plot(f(1:limit),plotY(1:limit));

subplot(2,2,2);
mirSa = 20 * log10(mirgetdata(mirspectrum('flute2.wav')));
f2 = (0:length(mirSa)-1) .* (22050/length(mirSa));

plot(f2, mirSa);

disp(length(mirSa));
disp(length(Y(1:limit)));
%--- CODE FOR PLOTTING
%}

sa = Y(1:limit);

%sound(audio,SR);
end

function sf = sfSub(spectralAnalysis)
sf = geomean(spectralAnalysis) / mean(spectralAnalysis);
end

function sf = spectralFlatness(spectralAnalysis)

[r,~] = size(spectralAnalysis);
sf = zeros(r,1);

for i = 1:r;
    sf(i) = sfSub(spectralAnalysis(i,:));
end

sf = mean(sf);

end

function sc = spectralCentroid_first(spectralAnalysis)

resolution = 22050 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));

sc = sum(spectralAnalysis .* frequencies') / sum (spectralAnalysis);
end

function [sc, sc_var] = spectralCentroid(spectralAnalysis)

[r, c] = size(spectralAnalysis);
resolution = 22050 / c;
frequencies = resolution .* (0:(c-1));
centroids = zeros(r,1);

for i = 1:r
centroids(i) = sum(spectralAnalysis(i,:) .* frequencies) / sum (spectralAnalysis(i,:));
end

sc_var = var(centroids);
sc = mean(centroids);
%disp(spectralAnalysis);
end

function scv = spectralCentroidVariance(spectralAnalysis)

resolution = 20000 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));

scv = sum(spectralAnalysis .* frequencies') / sum (spectralAnalysis);

end

function sp = strongPeak(spectralAnalysis)

[~,maxIndex] = max(spectralAnalysis);

threshold = spectralAnalysis(maxIndex) / 2.0;
resolution = 20000 / length(spectralAnalysis);

bandwidthLeft = maxIndex;
while (bandwidthLeft-1 >= 1 && spectralAnalysis(bandwidthLeft-1) > threshold)
    bandwidthLeft = bandwidthLeft - 1;
end


bandwidthRight = maxIndex;
while (bandwidthRight+1 <= length(spectralAnalysis) && spectralAnalysis(bandwidthRight+1) > threshold)
    bandwidthRight = bandwidthRight + 1;
end

sp = resolution * spectralAnalysis(maxIndex) / log10(bandwidthRight / bandwidthLeft);

end

function sk = spectralKurtosis(spectralAnalysis, centroid)

[r, c] = size(spectralAnalysis);

resolution = 22050 / c;
frequencies = resolution .* (0:c-1);

sk = zeros(r,1);
for i = 1:r
xn = spectralAnalysis(i,:) ./ sum(spectralAnalysis(i,:));
freqSubCentroid = frequencies - centroid;

nomenator = sum(xn .* (freqSubCentroid .^ 4));
spectralSpread = sum(xn .* (freqSubCentroid .^ 2));

sk(i) = nomenator / (spectralSpread^2);

end

sk = mean(sk);

end

function sk = spectralKurtosis_old(spectralAnalysis, centroid)

resolution = 22050 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));


xn = spectralAnalysis ./ sum(spectralAnalysis);
freqSubCentroid = frequencies - centroid;

nomenator = sum(xn .* (freqSubCentroid .^ 4)');
spectralSpread = sum(xn .* (freqSubCentroid .^ 2)');

sk = nomenator / (spectralSpread^2);

end

function ss = spectralSkewness(spectralAnalysis, centroid)

[r, c] = size(spectralAnalysis);

resolution = 22050 / c;
frequencies = resolution .* (0:c-1);

skewness = zeros(r,1);

for i = 1:r
xn = spectralAnalysis(i,:) ./ sum(spectralAnalysis(i,:));
freqSubCentroid = frequencies - centroid;

nomenator = sum(xn .* (freqSubCentroid .^ 3));
spectralSpread = sum(xn .* (freqSubCentroid .^ 2));

skewness(i) = nomenator / (spectralSpread^1.5);

end

ss = mean(skewness);

end

function sk = spectralSkewness_old(spectralAnalysis, centroid)

resolution = 20000 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));


xn = spectralAnalysis ./ sum(spectralAnalysis);
freqSubCentroid = frequencies - centroid;

nomenator = sum(xn .* (freqSubCentroid .^ 3)');
spectralSpread = sum(xn .* (freqSubCentroid .^ 2)');

sk = nomenator / (spectralSpread^1.5);

end

function ss = spectralSpread(spectralAnalysis, centroid)

resolution = 20000 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));

xn = spectralAnalysis ./ sum(spectralAnalysis);
freqSubCentroid = frequencies - centroid;

ss = sum(xn .* (freqSubCentroid .^ 2)');
end

function rs = energySpectral(spectralAnalysis, audio)

resolution = 20000 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));
frequencies = frequencies .^ 0.5;

xn = spectralAnalysis ./ sum(spectralAnalysis);
xn = xn .* frequencies';
rs = sum(xn);

%WRONG

vec = (spectralAnalysis .* sqrt(frequencies')) ;
vec = vec .* resolution;

%WRONG agin?

vec = spectralAnalysis .^ 2;

rs = sum(vec);

%WRONG AGAIN

rs = energy(spectralAnalysis) * sqrt(length(audio));
end

function eb = energyBands_old(audio, SR, file, windowLength, from, to)


%audio = [0.5 0.4 -0.4 -0.5 0.5 0.4 -0.4 -0.5 0.5 0.4 -0.4 -0.5 0.5 0.4 -0.4 -0.5]';

N = length(audio);
WindowLength = windowLength; %floor(N/2-2);
WindowLength2  = floor(WindowLength/2);
vector = zeros(ceil(N/WindowLength),WindowLength2);
w = hanningz(WindowLength);

idx = 1;
pin  = 0;


while pin < N - WindowLength
    %if(pin + WindowLength > N)
    %    WindowLength = N - pin;
    %end
    grain = audio(pin+1:pin+WindowLength);
    vector(idx, 1:WindowLength2) = spectAnSub(grain, w);
    pin = pin + WindowLength;
    idx = idx + 1;
end





%testEnergySpectralSub(audio, SR, file);
%disp(energy(vector));

%vector = [1 3 2; 4 2 5; 6 1 4; 1 1 1];
%Y = sum(vector);
%disp(Y);
vector = vector(1:idx-1,1:WindowLength2);
[r,c] = size(vector);

Y = zeros(1,c);
for i = 1:c
    Y(i) = energy(vector(1:r,i));
end


%ENERGY
e = energy(Y) / (sqrt(WindowLength) * energy (w));
%COMPENSATE
e = e * 0.5 * sum(w);
eb = e;
%disp(e);
%disp(energy(audio));

%

%Y = vector(4,1:8193);

%{
%--- CODE FOR PLOTTING
limit = length(Y); %round(N * (22050/SR));
subplot(2,2,1);
f = SR/WindowLength .* (0:WindowLength-1);
f = f(1:WindowLength);
plotY = Y; plotY = 20 * log10(Y); % turn spec logarithmic
plot(f(1:limit),plotY(1:limit));

subplot(2,2,2);
mirSa = 20 * log10(mirgetdata(mirspectrum(file)));
f2 = (0:length(mirSa)-1) .* (22050/length(mirSa));

plot(f2, mirSa);



%--- CODE FOR PLOTTING
%}

end

function eb = energyBands(sa, w, frameSize, from, to)
[~, c] = size(sa);
resolution = 22050/c;

from = round(from / resolution);
to = round(to / resolution);

sa(:,1:from) = 0;
sa(:,to:c) = 0;

%plotSpectrum(audio, sa, frameSize, 44100);

Y = toVec(sa);

%ENERGY
e = energy(Y) / (sqrt(frameSize) * energy (w));
%COMPENSATE
e = e * 0.5 * sum(w);
eb = e;

end

function Y = spectAnSub(audio, w)
%disp(energy(audio));

N = length(audio);

norm = N / sum(w);
audio = audio .* w;

Y = fft(audio);
Y = abs( Y(1:N) ) ./ (0.5*N);
%Normalize to compensate for hanningz window
Y = Y .* norm; %-9.0352
Y(Y==0) = eps; % avoiding log(0) --> >> help eps

%Cut spectrum in half, beacuse Nyquist!
limit = floor(N * 0.5);
Y = Y(1:limit);

%disp(Y);

%ENERGY
%e = energy(Y) / sqrt(N);
%e = e / energy (w);

%COMPENSATE
%e = e * 0.5 * N;
%e = e / norm;
%disp(e);

end

function sd = strongDecay(audio, SR)
    tc = temporalCentroid(audio,SR, 1024, 256);
    
    e = energy(audio);
    
    sd = sqrt(e/tc);
    
end

function [means, vars] = getMfcc(audio, sr)

ce = mfcc(audio, sr);
% Scale them to match (log_10 and power)
%ce = log(10)*2*ce;
% Duplicate with melfcc.m

% Plot... or not
%subplot(311)
%imagesc(ce(1:13,:)); axis xy; colorbar
%title('Auditory Toolbox MFCC');

means = 1:13;
vars = 1:13;
%ce(ce==-Inf | isnan(ce)) = 0;

for i = 1:13
    %subplot(5,3,i);
    vector = ce(i,:,:);
    %plot(vector);
    vector(vector==-Inf | isnan(vector)) = [];
    
    
    means(i) = mean(vector);
    vars(i) = var(vector);
end


end


function hejsan(file)
file = 'sd.wav';
% Load a speech waveform
[audio,sr] = audioread(file);

%audio = wgn(length(audio),1,100);
% Look at its regular spectrogram
%subplot(411)
%specgram(audio, 256, sr);
%audio = sinWav(3000,10);
%sound(audio,sr);
ce = mfcc(audio, sr);
% Scale them to match (log_10 and power)
%ce = log(10)*2*ce;
% Duplicate with melfcc.m

% .. and compare:
subplot(311)
imagesc(ce(2:13,:)); axis xy; colorbar
title('Auditory Toolbox MFCC');

meanVals = 1:13;
varVals = 1:3;
for i = 1:13
    subplot(5,3,i);
    vector = ce(i,:,:);
    plot(vector);
    vector(vector==-Inf | isnan(vector)) = [];
    
    meanVals(i) = mean(vector);
    varVals(i) = var(vector);
end

disp(meanVals');
disp(varVals');


vec = [NaN, 2, 3, 4, 5, 6];
vec((vec == 5) | (vec==3)) = [];
disp(vec');
     %ce(2,:,:)'


%{
 tic
[ceps,freqresp,fb,fbrecon,freqrecon] = mfcc(audio, sr, sr/(1024));
toc
subplot(3,2,1);
plot(ceps);

subplot(3,2,2);
plot(freqresp);

subplot(3,2,3);
plot(fb);

subplot(3,2,4);
plot(fbrecon);

subplot(3,2,5);
plot(freqrecon);
sound(audio(1:floor(length(audio)*0.1)),sr);
%}
end

function plotSpectrum(audio, spectrum, frameSize, SR)

%PLOT VECTOR
[r,c] = size(spectrum);
Y = 20 * log10(spectrum);
millis = floor(length(audio)/SR);
imagesc([0 millis],[0 SR/2],Y'); axis xy; colorbar;
%}
%disp(length(audio));
%disp(r);

%Y = vector;
%Y = sumTimeVector(vector);
%Y = 20 * log10(Y);

end

function w = whitenoise()
w = wgn(44100,1,-20);

end

function a = sinWav(freq,sec)

sr = 44100;
sampleSize = 1/sr;
t = 0:sampleSize:sec;
audio = 0.353 * sin(2*pi*freq*t);
%plot(t,audio);
%disp(t);
%sound(audio,sr);
a = audio;
end

