classdef Descriptors
    methods (Static)
        function descriptors = extract(audio, SR, sec, frameSizes, hopSizes, normalize)
            %file = 'sd.wav';
            %sec = 12;
            %audio = toMono(audio);
            if normalize
                audio = normalizeAudio(audio);
            end
            
            if sec ~= 0
                audio = cutSound(audio,SR, sec);
            end
            
            %audio = audio(ceil(length(audio)/2):length(audio));
            %audio = whitenoise();
            %audio = [audio' audio']';
            %audio = sinWav(440,20)';
            
            %disp(attackLength(from, to , SR));
            
            [atcFrom, atcTo] = attackIndex(audio);
            
            dcyFrom = atcTo;
            dcyTo = length(audio);
            
            %ATTACK DESCRIPTORS
            [sFrom,sTo] = attackTime(atcFrom, atcTo, SR);
            frame = frameSizes(1);
            hop = hopSizes(1);
            
            %
            % #1 ATTACK ENERGY
            atc_e = energy(audio(atcFrom:atcTo));
            
            
            % #2 TEMPORAL CENTROID
            atc_tc = temporalCentroid(audio(atcFrom:atcTo),SR,frame, hop);
            
            % #3  LOG ATTACK-TIME
            atc_lat = logAttackTime(atcFrom, atcTo, SR);
            
            % #4  ATTACK ZERO-CROSSING RATE
            atc_zcr = zeroCrossingRate(audio(atcFrom:atcTo));
            
            % #5  TC/EA
            atc_tcea = TC_EA(atcFrom, atcTo, SR, atc_tc);
            
            
            %DECAY DESCRIPTORS
            frame = frameSizes(2);
            hop = hopSizes(2);
            sa = spectralAnalysis(audio(dcyFrom:dcyTo), SR, frame, hop);
            %saVector = spectralAnalysisVector(audio,SR);
            
            % #6  DECAY SPECTRAL FLATNESS
            dcy_sf = spectralFlatness(sa);
            
            % #7  DECAY SPECTRAL CENTROID
            %centroid_first = spectralCentroid(sa);
            [dcy_sc, dcy_scv] = spectralCentroid(sa,SR);
            
            
            % #8  DECAY STRONG PEAK
            dcy_sp = strongPeak(toVec(sa), SR);
            
            % #9  DECAY SPECTRAL KURTOSIS
            %sk = spectralKurtosis_old(saVector, sc);
            dcy_sk = spectralKurtosis(sa, dcy_sc,SR);
            
            % #10 DECAY ZERO-CROSSING RATE
            %dcy_zcr = zeroCrossingRate(audio);
            [dcy_zcr, dcy_zcrVar] = zeroCrossingRateVar(audio(dcyFrom:dcyTo),frame);
            
            % #11 STRONG DECAY %framesize hardcoded!
            dcy_sd = strongDecay(audio(dcyFrom:dcyTo), SR, 1024, 256);
            
            % #12 DECAY SPECTRAL CENTROID VARIANCE
            %TAKE dcy_scv from #7!
            
            % #13 DECAY ZERO-CROSSING RATE VARIANCE
            %TAKE dcy_zcrVar from #10!
            
            % #14 DECAY SKEWNESS
            %dcy_ss = spectralSkewness_old(sa,dcy_sc);
            dcy_ss = spectralSkewness(sa,dcy_sc,SR);
            
            % #15 - #22
            frame = frameSizes(3);
            hop = hopSizes(3);
            aeb = allEnergyBands(audio, SR, frame, hop);
            
            
            % Mel-Frequency Cepstrum Coefficients (mean values) %
            % means #23 - #35, vars #36 - #48
            [means, vars] = getMfcc(audio, SR);
            
            descriptors = [atc_e atc_tc atc_lat atc_zcr atc_tcea ...
                dcy_sf dcy_sc dcy_sp dcy_sk dcy_zcr dcy_sd dcy_scv ...
                dcy_zcrVar dcy_ss aeb means vars];
            
        end
        
        function plotMfcc(audio, SR, name)
           audio = normalizeAudio(audio);
           %ce = mfcc(audio, SR,150);
           
            Tw = 25;           % analysis frame duration (ms)
            Ts = 10;           % analysis frame shift (ms)
            alpha = 20;      % preemphasis coefficient
            R = [ 20 20000 ];  % frequency range to consider
            M = 40;            % number of filterbank channels 
            C = 13;            % number of cepstral coefficients
            L = 40;            % cepstral sine lifter parameter
            hamming = @(N)(0.54-0.46*cos(2*pi*(0:N-1).'/(N-1)));
            [ CC, ~, ~ ] = mfcc( audio, SR, Tw, Ts, alpha, hamming, R, M, C, L );
            ce = CC;
            
            
            %{
            T = 25;           % analysis frame duration (ms)
            Ts = 10;           % analysis frame shift (ms)
            alpha = 0.97;      % preemphasis coefficient
            R = [ 20 20000 ];  % frequency range to consider
            M = 20;            % number of filterbank channels 
            C = 13;            % number of cepstral coefficients
            L = 22;            % cepstral sine lifter parameter
            hamming = @(N)(0.54-0.46*cos(2*pi*(0:N-1).'/(N-1)));
            [ CC, ~, ~ ] = mfcc( audio, SR, Tw, Ts, alpha, hamming, R, M, C, L );
            ce = CC;
            %}
            
            
            % Scale them to match (log_10 and power)
            %ce = 20 * log10(ce);
            % Duplicate with melfcc.m
            
            vector = ce(1,:);
            vector(vector == -Inf | isnan(vector)) = [];
            matrix = zeros(13, length(vector));
            for i = 1:13
                vector = ce(i,:);
                vector(vector == -Inf | isnan(vector)) = [];
                
                matrix(i,:) = vector;
            end
            [r,c] = size(matrix);
            
            % subplot(311)
            %clims = [-50 5];
            imagesc(0:c*10,1:r,matrix); axis xy; colorbar; colormap('jet');
            set(gca,'FontSize',14);
            string = sprintf('MFCCs of %s', name);
            title(string,'FontSize', 16);
            xlabel('Milliseconds (ms)','FontSize', 16) % x-axis label
            ylabel('Mel-frequency cepstral coefficients (MFCCs)','FontSize', 16) % y-axis label
            
        end
        
        function plotSpectrum(audio, SR, frameSize, hop, string, len)
            audio = normalizeAudio(audio);
            padding = zeros(1024*8,1);
            audio = vertcat(padding,audio);
            
            if(len ~= 0)
                audio = audio(1:floor(len*SR));
            end
            
            spectrum = spectralAnalysis(audio, SR, frameSize, hop);
            %spectrum = spectrum * 20;
            spectrum = normalizeSpectrum(spectrum);
            
            %PLOT VECTOR
            [r,c] = size(spectrum);
            %spectrum(spectrum < 1e-10) = 1e-10;
            
            %For db it's suppose be 20 //TODO
            Y =  20 * log10(spectrum);
            millis = floor(1000*length(audio)/SR);
            clims = [-60 0];
            Y = Y';
            %Y = expandMatrix(Y',floor(1000/r));
            
            imagesc([0 millis/1000],[0 (SR/2)/1000],Y, clims); axis xy; colorbar; colormap('jet');
            set(gca,'FontSize',14);
            string = sprintf('Spectral analysis of %s', string);
            title(string, 'FontSize', 16);
            xlabel('Time (s)', 'FontSize', 16) % x-axis label
            ylabel('Frequency (kHz)', 'FontSize', 16) % y-axis label
            
            %logfsgram(audio,1024,SR);
            
            %}
            %disp(length(audio));
            %disp(r);
            
            %Y = vector;
            %Y = sumTimeVector(vector);
            %Y = 20 * log10(Y);
            
        end
        
        function a = toMono(audio)
            [r,c] = size(audio);
            if(c == 2)
                audio = (audio(:,1)+audio(:,2))/2;
            end
            
            a = audio;
        end
        
        %Might implement this later
        function audioLength(audio, SR)
            disp('lol');
            rmsVector(audio,SR);
        end
        
    end
end

function matrix = expandMatrix(m, n)
if n > 1
    [r, c] = size(m);
    matrix = zeros(r,n*c);
    for i = 1:(c*n)
        
        matrix(:,i) = m(:,ceil(i/n));
    end
else
    matrix = m;
end
end

function audio = normalizeAudio(audio)
    m = max(abs(audio));
    audio = audio / m;
end

function spectrum = normalizeSpectrum(spectrum)
            [maxval, ~] = max(spectrum(:));

            spectrum = spectrum / maxval;
end

function rms = rmsVector(audio, SR)

DAFx_in = audio;
hop            = 256;  % hop size between two FFTs
WindowLength   = 1024; % length of the windows
w              = hanningz(WindowLength);


%----- some initialisations -----
WindowLength2  = WindowLength/2;
normW          = norm(w,2);
pft            = 1;
lf             = floor((length(DAFx_in) - WindowLength)/hop);
feature_rms    = zeros(lf,1);


tic
%===========================================
pin  = 0;
pend = length(DAFx_in) - WindowLength;

while pin<pend
    % A grain is a windowed part of the input signal
    grain = DAFx_in(pin+1:pin+WindowLength).* w;
    feature_rms(pft) = norm(grain,2) / normW;
    pft   = pft + 1;
    
    
    pin   = pin + hop;
end
% ===========================================
toc


n_signal = 1:pend;
t_signal = (n_signal-1)/SR; % time in sec

%pft is one longer than the size (and the size is one longer than required)
%hop/SR is the amount of seconds past per hop
t_rms = (0:pft-2)*hop/SR;

plot(t_rms, feature_rms);


end

function audio = cutSound(audio,SR, sec)

silence = sec * SR - length(audio);

silence = zeros(silence, 1);

audio = vertcat(audio, silence);

end

function aeb = allEnergyBands(audio, SR, frameSize, hop)

sa = spectralAnalysis(audio, SR, frameSize, hop);
w = hanningz(frameSize);

eb1 = energyBands(sa,w, frameSize, 40, 70, SR);
eb2 = energyBands(sa,w, frameSize, 70, 110, SR);
eb3 = energyBands(sa,w, frameSize, 130, 145, SR);
eb4 = energyBands(sa,w, frameSize, 160, 190, SR);
eb5 = energyBands(sa,w, frameSize, 300, 400, SR);
eb6 = energyBands(sa,w, frameSize, 5000, 7000, SR);
eb7 = energyBands(sa,w, frameSize, 7000, 10000, SR);
eb8 = energyBands(sa,w, frameSize, 10000, 15000, SR);

aeb = [eb1 eb2 eb3 eb4 eb5 eb6 eb7 eb8];

aeb = aeb / sum(aeb);

end

function spectrum = spectralAnalysis(audio, SR, frameSize, hop)

N = length(audio);
%frameSize = 1024*2;
WindowLength2  = floor(frameSize/2);
w = hanningz(frameSize);
%hop = 512;
spectrum = zeros(floor(N/hop),WindowLength2);

idx = 1;
pin  = 0;


while pin < N - frameSize
    
    grain = audio(pin+1:pin+frameSize);
    spectrum(idx, :) = spectAnSub(grain, w);
    pin = pin + hop;
    idx = idx + 1;
end

%Trim vector
spectrum = spectrum(1:idx-1, :);
%spectrum = normalizeSpectrum(spectrum);


%{
%PLOT VECTOR
Y = 20 * log10(spectrum);
millis = floor(1 * length(audio)/SR);
imagesc([0 millis],[0 SR/2],Y'); axis xy; colorbar;
%}

%Y = vector;
%Y = sumTimeVector(vector);
%Y = 20 * log10(Y);

end

function Y = toVec(matrix)

[r,c] = size(matrix);

Y = zeros(1,c);
for i = 1:c
    Y(i) = energy(matrix(1:r,i));
end



end

%Calculates the rms for an audio signal. No windowing needed.
function e = energy(audio)
%Length of vector could be: length(audio(from:to)),
%but it is also to - from + 1
e = norm(audio,2) / sqrt(length(audio));

end

function [attackBegin, attackEnd]  = attackIndex(audio)

%It's safe to assume the attack to be within the first fourth of the sound
%This improved some few extractions where audio was impaired in the end
peak = max(abs(audio(1:floor(length(audio)/4))));
attackBegin = find(abs(audio) >= peak*0.2,1,'first');
attackEnd = find(abs(audio) >= peak*0.8,1,'first');
if(attackBegin == attackEnd)
    attackEnd = attackBegin + 1;
end

end

function TC = temporalCentroid(audio,SR, WindowLength, hop)

DAFx_in = audio;

w              = hanningz(WindowLength);


%----- some initialisations -----
WindowLength2  = WindowLength/2;
normW          = norm(w,2);
pft            = 1;
lf             = ceil((length(DAFx_in) - WindowLength)/hop);
feature_rms    = zeros(lf,1);

%tic
%===========================================
pin  = 0;
pend = length(DAFx_in) - WindowLength;

while pin<pend
    % A grain is a windowed part of the input signal
    grain = DAFx_in(pin+1:pin+WindowLength).* w;
    feature_rms(pft) = norm(grain,2) / normW;
    pft   = pft + 1;
    
    pin   = pin + hop;
end
% ===========================================
%toc
feature_rms = feature_rms(1:pft-1);


t_rms = (0:pft-2)*hop/SR;



TC = sum(feature_rms .* (t_rms')) / sum(feature_rms);

if(isnan(TC))
    TC = 0;
end
end

function [sFrom,sTo] = attackTime(from, to, SR)
sFrom = from/SR;
sTo = to/SR;
end

function lat = logAttackTime(from, to, SR)
lat = log((to - from) / SR);
end

function zc = zeroCrossingRate(audio)

zc = 0;

for i = 2:length(audio)
    if(audio(i) * audio(i-1) < 0)
        zc = zc + 1;
    end
end

end

function [zc, zc_var] = zeroCrossingRateVar(audio, frameSize)

N = length(audio);
zCrossings = zeros(floor(N/frameSize), 1);

idx = 1;
pin = 0;

while pin < N - frameSize
    grain = audio(pin+1:pin+frameSize);
    zCrossings(idx) = zeroCrossingRate(grain);
    
    pin = pin + frameSize-1;
    idx = idx + 1;
end

%zCrossings(idx) = zeroCrossingRate(audio(pin:N));
%zCrossings = zCrossings(1:idx-1);

zc_var = var(zCrossings);
zc = sum(zCrossings);

end

function tc_ea = TC_EA( from, to, SR, TC)
attackLength = (to-from)/SR;
tc_ea = TC/attackLength;
end

function sf = sfSub(spectralAnalysis)
sf = geomean(spectralAnalysis) / mean(spectralAnalysis);
end

function sf = spectralFlatness(spectralAnalysis)

[r,~] = size(spectralAnalysis);
sf = zeros(r,1);

for i = 1:r;
    sf(i) = sfSub(spectralAnalysis(i,:));
end

sf = mean(sf);

end

function [sc, sc_var] = spectralCentroid(spectralAnalysis,SR)

[r, c] = size(spectralAnalysis);
resolution = SR * 0.5 / c;
frequencies = resolution .* (0:(c-1));
centroids = zeros(r,1);

for i = 1:r
    centroids(i) = sum(spectralAnalysis(i,:) .* frequencies) / sum (spectralAnalysis(i,:));
end

sc_var = var(centroids);
sc = mean(centroids);
end

function sp = strongPeak(spectralAnalysis, SR)

[~,maxIndex] = max(spectralAnalysis);

threshold = spectralAnalysis(maxIndex) / 2.0;
resolution = SR * 0.5 / length(spectralAnalysis);

bandwidthLeft = maxIndex;
while (bandwidthLeft-1 >= 1 && spectralAnalysis(bandwidthLeft-1) > threshold)
    bandwidthLeft = bandwidthLeft - 1;
end


bandwidthRight = maxIndex;
while (bandwidthRight+1 <= length(spectralAnalysis) && spectralAnalysis(bandwidthRight+1) > threshold)
    bandwidthRight = bandwidthRight + 1;
end

%This is to prevent infinity
%if they are the same, it will result in log10(1) = 0
if(bandwidthRight == bandwidthLeft)
    bandwidthRight = bandwidthRight + 1;
end

sp = resolution * spectralAnalysis(maxIndex) / log10(bandwidthRight/ bandwidthLeft);

end

function sk = spectralKurtosis(spectralAnalysis, centroid, SR)

[r, c] = size(spectralAnalysis);

resolution = SR * 0.5 / c;
frequencies = resolution .* (0:c-1);

sk = zeros(r,1);
for i = 1:r
    %Normalize the magnitude
    xn = spectralAnalysis(i,:) ./ sum(spectralAnalysis(i,:));
    
    
    freqSubCentroid = frequencies - centroid;
    
    nomenator = sum(xn .* (freqSubCentroid .^ 4));
    spectralSpread = sum(xn .* (freqSubCentroid .^ 2));
    
    sk(i) = nomenator / (spectralSpread^2);
    
end

sk = mean(sk);

end

function ss = spectralSkewness(spectralAnalysis, centroid, SR)

[r, c] = size(spectralAnalysis);

resolution = SR * 0.5 / c;
frequencies = resolution .* (0:c-1);

skewness = zeros(r,1);

for i = 1:r
    xn = spectralAnalysis(i,:) ./ sum(spectralAnalysis(i,:));
    freqSubCentroid = frequencies - centroid;
    
    nomenator = sum(xn .* (freqSubCentroid .^ 3));
    spectralSpread = sum(xn .* (freqSubCentroid .^ 2));
    
    skewness(i) = nomenator / (spectralSpread^1.5);
    
end

ss = mean(skewness);

end

function eb = energyBands(sa, w, frameSize, from, to, SR)
[~, c] = size(sa);
resolution = SR * 0.5/c;

from = round(from / resolution);
to = round(to / resolution);

sa(:,1:from) = 0;
sa(:,to:c) = 0;

%plotSpectrum(audio, sa, frameSize, 44100);

Y = toVec(sa);

%ENERGY
e = energy(Y);
%COMPENSATE technically this needs to be 
%performed in for this to approximately hold true: e == energy(audio)
%e = e / (sqrt(frameSize) * energy (w));
%e = e * 0.5 * sum(w);

%However, since the energy bands are relative this should not matter

eb = e;

end

function Y = spectAnSub(audio, w)
%disp(energy(audio));

N = length(audio);

norm = N / sum(w);
audio = audio .* w;

Y = fft(audio);
Y = abs( Y(1:N) ) ./ (0.5*N);
%Normalize to compensate for hanningz window
Y = Y .* norm; %-9.0352
Y(Y==0) = eps; % avoiding log(0) --> >> help eps

%Cut spectrum in half, beacuse Nyquist!
limit = floor(N * 0.5);
Y = Y(1:limit);

%disp(Y);

%ENERGY
%e = energy(Y) / sqrt(N);
%e = e / energy (w);

%COMPENSATE
%e = e * 0.5 * N;
%e = e / norm;
%disp(e);

end

function sd = strongDecay(audio, SR,frame,hop)
tc = temporalCentroid(audio,SR, frame, hop);

e = energy(audio);

sd = sqrt(e/tc);

end

function [means, vars] = getMfcc(audio, sr)

%ce = mfcc(audio, sr);

            Tw = 25;           % analysis frame duration (ms)
            Ts = 10;           % analysis frame shift (ms)
            alpha = 20;      % preemphasis coefficient
            R = [ 20 20000 ];  % frequency range to consider
            M = 40;            % number of filterbank channels 
            C = 13;            % number of cepstral coefficients
            L = 40;            % cepstral sine lifter parameter
            hamming = @(N)(0.54-0.46*cos(2*pi*(0:N-1).'/(N-1)));
            [ CC, ~, ~ ] = mfcc( audio, sr, Tw, Ts, alpha, hamming, R, M, C, L );
            ce = CC;
            
            
            %{
            T = 25;           % analysis frame duration (ms)
            Ts = 10;           % analysis frame shift (ms)
            alpha = 0.97;      % preemphasis coefficient
            R = [ 20 20000 ];  % frequency range to consider
            M = 20;            % number of filterbank channels 
            C = 13;            % number of cepstral coefficients
            L = 22;            % cepstral sine lifter parameter
            hamming = @(N)(0.54-0.46*cos(2*pi*(0:N-1).'/(N-1)));
            [ CC, ~, ~ ] = mfcc( audio, SR, Tw, Ts, alpha, hamming, R, M, C, L );
            ce = CC;
            %}
            
            
% Scale them to match (log_10 and power)
%ce = log(10)*2*ce;
% Duplicate with melfcc.m

% Plot... or not
%subplot(311)
%imagesc(ce(1:13,:)); axis xy; colorbar
%title('Auditory Toolbox MFCC');

means = 1:13;
vars = 1:13;
%ce(ce==-Inf | isnan(ce)) = 0;

for i = 1:13
    
    vector = ce(i,:,:);
    
    %The mfcc is a 1x799 vector, sometimes, a couple of values will be -Inf
    %or NaN. This row makes sure this never happens.
    vector(vector==-Inf | isnan(vector)) = [];
    
    
    means(i) = mean(vector);
    vars(i) = var(vector);
end


end

function w = whitenoise()
w = wgn(44100,1,-20);

end

function a = sinWav(freq,sec)

sr = 44100;
sampleSize = 1/sr;
t = 0:sampleSize:sec;
audio = 0.353 * sin(2*pi*freq*t);
%plot(t,audio);
%disp(t);
%sound(audio,sr);
a = audio;
end
