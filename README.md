# Classification of drum sounds from sample libraries: An evaluation of features and classification techniques #

### Abstract ###
This thesis presents an evaluation of automatic instrument classification methods. The instruments specific for this study are acoustic drums from sample libraries. A model has been built for classifying drums. The classification problem has been evaluated in three different categories: The super-category (membrane and plate), the basic-category (cymbal, kick, snare, hi-hat and tom) and the sub-category (kick, snare, low & high tom, open & closed hi-hat, china, crash, ride bell and ride bow). The super category was divided into membrane instruments and plate instruments. The basic category was divided into kick, snare, tom, hi-hat, cymbal instruments. The sub categories was divided into kick, snare, low tom, high tom, open hi-hat, closed hi-hat, china cymbal, crash cymbal, ride bow and ride bell. The model consists of a feature extraction model, and six different machine learning classification techniques. A dataset of 3900 sounds have been used for training, and an additional disjunct dataset of 1000 sounds have been used for evaluation. High hit rates have been achieved for each different category (super, basic and sub): 100%, 96.6%, and 92.1%. The best hit rates was from the random forest classification technique. The most useful extracted features was mel-frequency cepstral coefficients, TC/EA (ratio between temporal centroid and attack length) and spectral related features.

##

**Docs link: ** 
* https://docs.google.com/document/d/1ze8GaQCRVykBUBeumzxUtstJlvhCKZ81V1sb7Mj-1cs/edit


-----



# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact