% M-file 9.25
% UX_centroid.m

% feature_centroid1 and 2 are centroids
% calculate by two different methods

clear;clf

%----- USER DATA -----
[DAFx_in, SR]  = wavread('flute2.wav');
hop            = 256;  % hop size between two FFTs
WindowLength   = 1024; % length of the windows
w              = hanningz(WindowLength);


%----- some initialisations -----
WindowLength2  = WindowLength/2;
tx             = (1:WindowLength2+1)';
normW          = norm(w,2);
coef           = (WindowLength/(2*pi));
pft            = 1;
lf             = floor((length(DAFx_in) - WindowLength)/hop);
feature_rms    = zeros(lf,1);
feature_centroid  = zeros(lf,1);
feature_centroid2 = zeros(lf,1);

tic
%===========================================
pin  = 0;
pend = length(DAFx_in) - WindowLength;

while pin<pend
   grain                  = DAFx_in(pin+1:pin+WindowLength).* w;
   feature_rms(pft)       = norm(grain,2) / normW;
   f                      = fft(grain)/WindowLength2;
   fx                     = abs(f(tx));
   feature_centroid(pft)  = sum(fx.*(tx-1)) / sum(fx);
   fx2                    = fx.*fx;
   feature_centroid2(pft) = sum(fx2.*(tx-1)) / sum(fx2);
   grain2                 = diff(DAFx_in(pin+1:pin+WindowLength+1)).* w;
   feature_deriv(pft)     = coef * norm(grain2,2) / norm(grain,2);
   pft                    = pft + 1;
   pin                    = pin + hop;
end
% ===========================================
toc

t_feature = (0:pft-2)*hop/SR;

subplot(4,1,1); 
plot(t_feature, feature_rms); 
axis([0 max(t_feature) 0 1])
ylabel('RMS')

subplot(4,1,2); 
plot(t_feature, feature_centroid); 
axis([0 max(t_feature) 0 50])
ylabel('centroid 1')

subplot(4,1,3); 
plot(t_feature, feature_centroid2); 
axis([0 max(t_feature) 0 50])
ylabel('centroid 2')

subplot(4,1,4); 
plot(t_feature, feature_deriv); 
axis([0 max(t_feature) 0 50])
ylabel('centroid 3')
xlabel('t/s \rightarrow')

