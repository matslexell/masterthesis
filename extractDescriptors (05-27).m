% M-file 9.24
% UX_rms.m

function extractDescriptors()
file = 'crash.wav';
frameSize = 1024;
hop = 512;

[audio, SR] = audioread(file);

%audio = [audio' audio']';
%audio = sinWav(440,20)';


[from,to] = attackIndex(audio);
[sFrom,sTo] = attackTime(from, to, SR);

%disp(attackLength(from, to , SR));

disp(from);
disp(to);



%
% #1 ATTACK ENERGY
e = energy(audio(from:to));

%disp(e);
%disp(mir);


% #2 TEMPORAL CENTROID
tc = temporalCentroid(audio(from:to),SR,frameSize, hop);

% #3  LOG ATTACK-TIME
logAttackTime(from, to, SR);

% #4  ATTACK ZERO-CROSSING RATE
zeroCrossingRate(audio(from:to));

% #5  TC/EA
TC_EA(from, to, SR, tc);


%DECAY DESCRIPTORS
sa = newSpectral(audio, SR, frameSize, hop);
sa = spectralAnalysis(audio,SR);

%e = norm(audio,2) / sqrt(length(audio));
e2 = energySpectral(sa, audio);
%disp(e2);


% #6  DECAY SPECTRAL FLATNESS
sf = spectralFlatness(sa);

% #7  DECAY SPECTRAL CENTROID
centroid = spectralCentroid(sa);


% #8  DECAY STRONG PEAK
strongPeak(sa);

% #9  DECAY SPECTRAL KURTOSIS
sk = spectralKurtosis(sa, centroid);


% #10 DECAY ZERO-CROSSING RATE
zeroCrossingRate(audio);

% #11 STRONG DECAY
strongDecay(audio, SR);

% #12 DECAY SPECTRAL CENTROID VARIANCE


% #13 DECAY ZERO-CROSSING RATE VARIANCE


% #14 DECAY SKEWNESS
spectralSkewness(sa,centroid);

%{
% ENERGY BANDS %
acc = 0;
WindowLength = 1024*16;
%
disp(energyBands(audio, SR, file, WindowLength, 0, 22050));

lol = energyBands(audio, SR, file, WindowLength, 0, 10000);
lol2 = energyBands(audio, SR, file, WindowLength, 10000, 20000);

disp(lol);
disp(lol2);

disp(lol + lol2);
% #15 40-70 Hz
e = energyBands(audio, SR, file, WindowLength, 40, 70);
acc = e + acc;
disp(e);
% #16 70-110 Hz
e = energyBands(audio, SR, file, WindowLength, 70, 110);
acc = e + acc;

disp(e);

% #17 130-145 Hz
e = energyBands(audio, SR, file, WindowLength, 130, 145);
acc = e + acc;

disp(e);

% #18 160-190 Hz
e = energyBands(audio, SR, file, WindowLength, 160, 190);
acc = e + acc;

disp(e);

% #19 300-400 Hz
e = energyBands(audio, SR, file, WindowLength, 300, 400);
acc = e + acc;

disp(e);

% #20 5-7 KHz
e = energyBands(audio, SR, file, WindowLength, 5000, 7000);
acc = e + acc;

disp(e);

% #21 7-10 KHz
e = energyBands(audio, SR, file, WindowLength, 7000, 10000);
acc = e + acc;

disp(e);

% #22 10-15 KHz
e = energyBands(audio, SR, file, WindowLength, 10000, 15000);
acc = e + acc;
disp(e);

disp(acc);
%}

% Mel-Frequency Cepstrum Coefficients (mean values) %
% #23s
[means, vars] = getMfcc(audio, SR);

% Mel-Frequency Cepstrum Coefficients (variances) %

%audio = audio(1:floor(length(audio)*0.1));
%sound(audio,SR);

%}

end

function spectrum = newSpectral(audio, SR, frameSize, hop)

N = length(audio);
%frameSize = 1024*2;
WindowLength2  = floor(frameSize/2);
w = hanningz(frameSize);
%hop = 512;
spectrum = zeros(floor(N/hop),WindowLength2);

idx = 1;
pin  = 0;


while pin < N - frameSize
    
    grain = audio(pin+1:pin+frameSize);
    spectrum(idx, :) = spectAnSub(grain, w);
    pin = pin + hop;
    idx = idx + 1;
end

%Trim vector
spectrum = spectrum(1:idx-1, :);

%{
%PLOT VECTOR
Y = 20 * log10(spectrum);
millis = floor(1 * length(audio)/SR);
imagesc([0 millis],[0 SR/2],Y'); axis xy; colorbar;
%}

%Y = vector;
%Y = sumTimeVector(vector);
%Y = 20 * log10(Y);

end

function Y = sumTimeVector(vector)

[r,c] = size(vector);
disp(r);
disp(c);
Y = zeros(1,c);
for i = 1:c
    Y(i) = energy(vector(1:r,i));
end



end

%Calculates the rms between the given idxs. No windowing needed.
function e = energy(audio)
%Length of vector could be: length(audio(from:to)),
%but it is also to - from + 1
e = norm(audio,2) / sqrt(length(audio));

end

function [attackBegin, attackEnd]  = attackIndex(audio)

peak = max(abs(audio));
attackBegin = find(abs(audio) >= peak*0.2,1,'first');
attackEnd = find(abs(audio) >= peak*0.8,1,'first');

end

function TC = temporalCentroid(audio,SR, WindowLength, hop)

DAFx_in = audio;

w              = hanningz(WindowLength);


%----- some initialisations -----
WindowLength2  = WindowLength/2;
normW          = norm(w,2);
pft            = 1;
lf             = floor((length(DAFx_in) - WindowLength)/hop);
feature_rms    = zeros(lf,1);


%tic
%===========================================
pin  = 0;
pend = length(DAFx_in) - WindowLength;

while pin<pend
    % A grain is a windowed part of the input signal
    grain = DAFx_in(pin+1:pin+WindowLength).* w;
    feature_rms(pft) = norm(grain,2) / normW;
    pft   = pft + 1;
    
    
    pin   = pin + hop;
end
% ===========================================
%toc


n_signal = 1:pend;
t_signal = (n_signal-1)/SR; % time in sec

t_rms = (0:pft-2)*hop/SR;



TC = sum(feature_rms.*(t_rms')) / sum(feature_rms);

%{
% REMOVE LATER //TODO
subplot(2,2,1);
plot(t_signal, DAFx_in(n_signal));
title('Signal')
xlabel('t/s \rightarrow')
%axis([0 max(t_signal) [-1.2 1.2]*max(abs(DAFx_in))])
axis([0 max(t_signal) -1 1])

subplot(2,2,2);
plot(t_rms, feature_rms);
title('RMS')
xlabel('t/s \rightarrow')

%}
end

function l = sec(audio, SR)
l = length(audio)/SR;
end

function l = attackLength(from,to,SR)
l = (to-from)/SR;
end

function [sFrom,sTo] = attackTime(from, to, SR)
sFrom = from/SR;
sTo = to/SR;
end

function lat = logAttackTime(from, to, SR)
lat = log((to - from) / SR);
end

function zc = zeroCrossingRate(audio)

zc = 0;

for i = 2:length(audio)
    if(audio(i) * audio(i-1) < 0)
        zc = zc + 1;
    end
end

end

function tc_ea = TC_EA( from, to, SR, TC)
attackLength = (to-from)/SR;
tc_ea = TC/attackLength;
end

function sa = spectralAnalysis(audio, SR)

%use WGN for creating white noise
%audio = wgn(length(audio),1,100);
N=length(audio);
w = hanningz(N);

f = SR/N .* (0:N-1);
f = f(1:N);

%Use a hanningz to avoid spectral leakage
audio = audio .* w;

%This is the normalizing facotor
norm = N / sum(w);

% transformation -> returns complex coefficients
Y = fft(audio, N);
% compute abs for plot and normalize to N
% Normalize to 0.5N instead... Because Nyquist!
Y = abs( Y(1:N) ) ./ (0.5*N);

%Normalize to compensate for hanningz window
Y = Y .* norm; %-9.0352
Y(Y==0) = eps; % avoiding log(0) --> >> help eps
limit = round(length(audio) * (22050/SR));



%{
%--- CODE FOR PLOTTING
subplot(2,2,1);
plotY = Y; plotY = 20 * log10(Y); % turn spec logarithmic
plot(f(1:limit),plotY(1:limit));

subplot(2,2,2);
mirSa = 20 * log10(mirgetdata(mirspectrum('flute2.wav')));
f2 = (0:length(mirSa)-1) .* (22050/length(mirSa));

plot(f2, mirSa);

disp(length(mirSa));
disp(length(Y(1:limit)));
%--- CODE FOR PLOTTING
%}

sa = Y(1:limit);

%sound(audio,SR);
end

function sf = spectralFlatness(spectralAnalysis)
sf = geomean(spectralAnalysis) / mean(spectralAnalysis);
end

function sc = spectralCentroid(spectralAnalysis)

resolution = 22050 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));

sc = sum(spectralAnalysis .* frequencies') / sum (spectralAnalysis);

end

function scv = spectralCentroidVariance(spectralAnalysis)

resolution = 20000 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));

scv = sum(spectralAnalysis .* frequencies') / sum (spectralAnalysis);

end

function sp = strongPeak(spectralAnalysis)

[~,maxIndex] = max(spectralAnalysis);

threshold = spectralAnalysis(maxIndex) / 2.0;
resolution = 20000 / length(spectralAnalysis);

bandwidthLeft = maxIndex;
while (bandwidthLeft-1 >= 1 && spectralAnalysis(bandwidthLeft-1) > threshold)
    bandwidthLeft = bandwidthLeft - 1;
end


bandwidthRight = maxIndex;
while (bandwidthRight+1 <= length(spectralAnalysis) && spectralAnalysis(bandwidthRight+1) > threshold)
    bandwidthRight = bandwidthRight + 1;
end

sp = resolution * spectralAnalysis(maxIndex) / log10(bandwidthRight / bandwidthLeft);

end

function sk = spectralKurtosis(spectralAnalysis, centroid)

resolution = 20000 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));


xn = spectralAnalysis ./ sum(spectralAnalysis);
freqSubCentroid = frequencies - centroid;

nomenator = sum(xn .* (freqSubCentroid .^ 4)');
spectralSpread = sum(xn .* (freqSubCentroid .^ 2)');

sk = nomenator / (spectralSpread^2);

end

function sk = spectralSkewness(spectralAnalysis, centroid)

resolution = 20000 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));


xn = spectralAnalysis ./ sum(spectralAnalysis);
freqSubCentroid = frequencies - centroid;

nomenator = sum(xn .* (freqSubCentroid .^ 3)');
spectralSpread = sum(xn .* (freqSubCentroid .^ 2)');

sk = nomenator / (spectralSpread^1.5);

end

function ss = spectralSpread(spectralAnalysis, centroid)

resolution = 20000 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));

xn = spectralAnalysis ./ sum(spectralAnalysis);
freqSubCentroid = frequencies - centroid;

ss = sum(xn .* (freqSubCentroid .^ 2)');
end

function rs = energySpectral(spectralAnalysis, audio)

resolution = 20000 / length(spectralAnalysis);
frequencies = resolution .* (0:(length(spectralAnalysis)-1));
frequencies = frequencies .^ 0.5;

xn = spectralAnalysis ./ sum(spectralAnalysis);
xn = xn .* frequencies';
rs = sum(xn);

%WRONG

vec = (spectralAnalysis .* sqrt(frequencies')) ;
vec = vec .* resolution;

%WRONG agin?

vec = spectralAnalysis .^ 2;

rs = sum(vec);

%WRONG AGAIN

rs = energy(spectralAnalysis) * sqrt(length(audio));
end

function eb = energyBands(audio, SR, file, windowLength, from, to)


%audio = [0.5 0.4 -0.4 -0.5 0.5 0.4 -0.4 -0.5 0.5 0.4 -0.4 -0.5 0.5 0.4 -0.4 -0.5]';

N = length(audio);
WindowLength = windowLength; %floor(N/2-2);
WindowLength2  = floor(WindowLength/2);
vector = zeros(ceil(N/WindowLength),WindowLength2);
w = hanningz(WindowLength);

idx = 1;
pin  = 0;


while pin < N - WindowLength
    %if(pin + WindowLength > N)
    %    WindowLength = N - pin;
    %end
    grain = audio(pin+1:pin+WindowLength);
    vector(idx, 1:WindowLength2) = spectAnSub(grain, w);
    pin = pin + WindowLength;
    idx = idx + 1;
end





%testEnergySpectralSub(audio, SR, file);
%disp(energy(vector));

%vector = [1 3 2; 4 2 5; 6 1 4; 1 1 1];
%Y = sum(vector);
%disp(Y);
vector = vector(1:idx-1,1:WindowLength2);
[r,c] = size(vector);

Y = zeros(1,c);
for i = 1:c
    Y(i) = energy(vector(1:r,i));
end


%ENERGY
e = energy(Y) / (sqrt(WindowLength) * energy (w));
%COMPENSATE
e = e * 0.5 * sum(w);
eb = e;
%disp(e);
%disp(energy(audio));

%

%Y = vector(4,1:8193);

%{
%--- CODE FOR PLOTTING
limit = length(Y); %round(N * (22050/SR));
subplot(2,2,1);
f = SR/WindowLength .* (0:WindowLength-1);
f = f(1:WindowLength);
plotY = Y; plotY = 20 * log10(Y); % turn spec logarithmic
plot(f(1:limit),plotY(1:limit));

subplot(2,2,2);
mirSa = 20 * log10(mirgetdata(mirspectrum(file)));
f2 = (0:length(mirSa)-1) .* (22050/length(mirSa));

plot(f2, mirSa);



%--- CODE FOR PLOTTING
%}

end

function Y = spectAnSub(audio, w)
%disp(energy(audio));

N = length(audio);

norm = N / sum(w);
audio = audio .* w;

Y = fft(audio);
Y = abs( Y(1:N) ) ./ (0.5*N);
%Normalize to compensate for hanningz window
Y = Y .* norm; %-9.0352
Y(Y==0) = eps; % avoiding log(0) --> >> help eps

%Cut spectrum in half, beacuse Nyquist!
limit = floor(N * 0.5);
Y = Y(1:limit);

%disp(Y);

%ENERGY
%e = energy(Y) / sqrt(N);
%e = e / energy (w);

%COMPENSATE
%e = e * 0.5 * N;
%e = e / norm;
%disp(e);

end

function sd = strongDecay(audio, SR)
    tc = temporalCentroid(audio,SR, 1024, 256);
    
    e = energy(audio);
    
    sd = sqrt(e/tc);
    
end

function [means, vars] = getMfcc(audio, sr)

ce = mfcc(audio, sr);
% Scale them to match (log_10 and power)
%ce = log(10)*2*ce;
% Duplicate with melfcc.m

% Plot... or not
%subplot(311)
%imagesc(ce(1:13,:)); axis xy; colorbar
%title('Auditory Toolbox MFCC');

means = 1:13;
vars = 1:13;
%ce(ce==-Inf | isnan(ce)) = 0;

for i = 1:13
    %subplot(5,3,i);
    vector = ce(i,:,:);
    %plot(vector);
    vector(vector==-Inf | isnan(vector)) = [];
    
    
    means(i) = mean(vector);
    vars(i) = var(vector);
end


end


function hejsan(file)
file = 'sd.wav';
% Load a speech waveform
[audio,sr] = audioread(file);

%audio = wgn(length(audio),1,100);
% Look at its regular spectrogram
%subplot(411)
%specgram(audio, 256, sr);
%audio = sinWav(3000,10);
%sound(audio,sr);
ce = mfcc(audio, sr);
% Scale them to match (log_10 and power)
%ce = log(10)*2*ce;
% Duplicate with melfcc.m

% .. and compare:
subplot(311)
imagesc(ce(2:13,:)); axis xy; colorbar
title('Auditory Toolbox MFCC');

meanVals = 1:13;
varVals = 1:3;
for i = 1:13
    subplot(5,3,i);
    vector = ce(i,:,:);
    plot(vector);
    vector(vector==-Inf | isnan(vector)) = [];
    
    meanVals(i) = mean(vector);
    varVals(i) = var(vector);
end

disp(meanVals');
disp(varVals');


vec = [NaN, 2, 3, 4, 5, 6];
vec((vec == 5) | (vec==3)) = [];
disp(vec');
     %ce(2,:,:)'


%{
 tic
[ceps,freqresp,fb,fbrecon,freqrecon] = mfcc(audio, sr, sr/(1024));
toc
subplot(3,2,1);
plot(ceps);

subplot(3,2,2);
plot(freqresp);

subplot(3,2,3);
plot(fb);

subplot(3,2,4);
plot(fbrecon);

subplot(3,2,5);
plot(freqrecon);
sound(audio(1:floor(length(audio)*0.1)),sr);
%}
end

function a = sinWav(freq,sec)

sr = 44100;
sampleSize = 1/sr;
t = 0:sampleSize:sec;
audio = 0.5 * sin(2*pi*freq*t);
%plot(t,audio);
%disp(t);
%sound(audio,sr);
a = audio;
end

