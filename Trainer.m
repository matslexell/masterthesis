function Trainer()



%[trainMatrixNorm,trainMatrix] = 
%genTestAndTrainComplete(14);
%genTestAndTrainCompleteN2(14);
genTestAndTrainCompleteN3(13);

%plotKMeansScatter(trainMatrix,10);


end

function [testset, trainset, labelsTest, labelsTrain] = genTestAndTrainSubset(trainName, testName, folder, idx, part)
    [china,crash,hiHatClosed,hiHatOpen,kick,rideBell,rideBow,snare,tomHigh,tomLow] = readData(folder,idx);

    [chinaTest, chinaTrain] = testTrain(china, part);
    [crashTest, crashTrain] = testTrain(crash, part);
    [hiHatClosedTest, hiHatClosedTrain] = testTrain(hiHatClosed, part);
    [hiHatOpenTest, hiHatOpenTrain] = testTrain(hiHatOpen, part);
    [kickTest, kickTrain] = testTrain(kick, part);
    [rideBellTest, rideBellTrain] = testTrain(rideBell, part);
    [rideBowTest, rideBowTrain] = testTrain(rideBow, part);
    [snareTest, snareTrain] = testTrain(snare, part);
    [tomHighTest, tomHighTrain] = testTrain(tomHigh, part);
    [tomLowTest, tomLowTrain] = testTrain(tomLow, part);
   
    
    labels = {'kick','tomLow','tomHigh','snare','hiHatClosed','hiHatOpen','crash','china','rideBow','rideBell'};
    labelsTest = createlabels({kickTest,tomLowTest,tomHighTest,snareTest,hiHatClosedTest,hiHatOpenTest,crashTest,chinaTest,rideBowTest,rideBellTest},labels);
    labelsTrain = createlabels({kickTrain,tomLowTrain,tomHighTrain,snareTrain,hiHatClosedTrain,hiHatOpenTrain,crashTrain,chinaTrain,rideBowTrain,rideBellTrain},labels);

    testset = [kickTest;tomLowTest;tomHighTest;snareTest;hiHatClosedTest;hiHatOpenTest;crashTest;chinaTest;rideBowTest;rideBellTest];
    trainset = [kickTrain;tomLowTrain;tomHighTrain;snareTrain;hiHatClosedTrain;hiHatOpenTrain;crashTrain;chinaTrain;rideBowTrain;rideBellTrain];
    
    testset = healAndNormalize(testset);
    trainset = healAndNormalize(trainset);

     writeMatrixAndLabels(testset,labelsTest, testName);
      writeMatrixAndLabels(trainset,labelsTrain, trainName);
      
end

function [trainMatrixNorm,trainMatrix] = genTestAndTrainComplete(idx)
[china,crash,hiHatClosed,hiHatOpen,kick,rideBell,rideBow,snare,tomHigh,tomLow] = readData('train',idx);


labels = {'kick','tomLow','tomHigh','snare','hiHatClosed','hiHatOpen','crash','china','rideBow','rideBell'};
labelsTrain = createlabels({kick,tomLow,tomHigh,snare,hiHatClosed,hiHatOpen,crash,china,rideBow,rideBell},labels);
trainMatrix = [kick;tomLow;tomHigh;snare;hiHatClosed;hiHatOpen;crash;china;rideBow;rideBell];

[china,crash,hiHatClosed,hiHatOpen,kick,rideBell,rideBow,snare,tomHigh,tomLow] = readData('test',idx);
labelsTest = createlabels({kick,tomLow,tomHigh,snare,hiHatClosed,hiHatOpen,crash,china,rideBow,rideBell},labels);
testMatrix = [kick;tomLow;tomHigh;snare;hiHatClosed;hiHatOpen;crash;china;rideBow;rideBell];



[range, means] = normValues(trainMatrix);
trainMatrixNorm = normalize(trainMatrix, range, means);
testMatrixNorm = normalize(testMatrix, range, means);


writeMatrixAndLabels(trainMatrixNorm,labelsTrain, 'trainSet');
writeMatrixAndLabels(testMatrixNorm,labelsTest, 'testSet');

end

function [trainMatrixNorm,trainMatrix] = genTestAndTrainCompleteN2(idx)
[china,crash,hiHatClosed,hiHatOpen,kick,rideBell,rideBow,snare,tomHigh,tomLow] = readData('train',idx);


labels = {'kick','tomLow','tomHigh','snare','hiHatClosed','hiHatOpen','crash','china','rideBow','rideBell'};
labelsTrain = createlabels({kick,tomLow,tomHigh,snare,hiHatClosed,hiHatOpen,crash,china,rideBow,rideBell},labels);
trainMatrix = [kick;tomLow;tomHigh;snare;hiHatClosed;hiHatOpen;crash;china;rideBow;rideBell];

[china,crash,hiHatClosed,hiHatOpen,kick,rideBell,rideBow,snare,tomHigh,tomLow] = readData('test',idx);
labelsTest = createlabels({kick,tomLow,tomHigh,snare,hiHatClosed,hiHatOpen,crash,china,rideBow,rideBell},labels);
testMatrix = [kick;tomLow;tomHigh;snare;hiHatClosed;hiHatOpen;crash;china;rideBow;rideBell];



[STDs, means] = normStdValues(trainMatrix);
trainMatrixNorm = normalizeStd(trainMatrix, STDs, means);
testMatrixNorm = normalizeStd(testMatrix, STDs, means);


writeMatrixAndLabels(trainMatrixNorm,labelsTrain, 'trainSet');
writeMatrixAndLabels(testMatrixNorm,labelsTest, 'testSet');

end

function [trainMatrixNorm,trainMatrix] = genTestAndTrainCompleteN3(idx)
[china,crash,hiHatClosed,hiHatOpen,kick,rideBell,rideBow,snare,tomHigh,tomLow] = readData('train',idx);


labels = {'kick','tomLow','tomHigh','snare','hiHatClosed','hiHatOpen','crash','china','rideBow','rideBell'};
labelsTrain = createlabels({kick,tomLow,tomHigh,snare,hiHatClosed,hiHatOpen,crash,china,rideBow,rideBell},labels);
trainMatrix = [kick;tomLow;tomHigh;snare;hiHatClosed;hiHatOpen;crash;china;rideBow;rideBell];

[china,crash,hiHatClosed,hiHatOpen,kick,rideBell,rideBow,snare,tomHigh,tomLow] = readData('test',idx);
labelsTest = createlabels({kick,tomLow,tomHigh,snare,hiHatClosed,hiHatOpen,crash,china,rideBow,rideBell},labels);
testMatrix = [kick;tomLow;tomHigh;snare;hiHatClosed;hiHatOpen;crash;china;rideBow;rideBell];



[range,mins] = normMinValues(trainMatrix);
trainMatrixNorm = normalizeMin(trainMatrix, range,mins);
testMatrixNorm = normalizeMin(testMatrix, range,mins);


writeMatrixAndLabels(trainMatrixNorm,labelsTrain, 'trainSet');
writeMatrixAndLabels(testMatrixNorm,labelsTest, 'testSet');

end


function genTestElectro(name)
[chinaT,crashT,hiHatClosedT,hiHatOpenT,kickT,rideBellT,rideBowT,snareT,tomHighT,tomLowT] = readData('train',1);
    %train = [chinaT;crashT;hiHatClosedT;hiHatOpenT;kickT;rideBellT;rideBowT;snareT;tomHighT;tomLowT];
    
    crash = dlmread('data\\electro_1\\crash.txt');
    hiHatClosed = dlmread('data\\electro_1\\hiHatClosed.txt');
    hiHatOpen = dlmread('data\\electro_1\\hiHatOpen.txt');
    kick = dlmread('data\\electro_1\\kick.txt');
    snare = dlmread('data\\electro_1\\snare.txt');
   
   labels = {'kick','snare','hiHatClosed','hiHatOpen','crash'};
    labels = createlabels({kick,snare,hiHatClosed,hiHatOpen,crash},labels);
    
    

    
    F = [kick;snare;hiHatClosed;hiHatOpen;crash];    
    
        [range, means] = normValues(F);
    F = normalize(F, range, means);
    

     writeMatrixAndLabels(F,labels, name);
      
end


function [test, train] = testTrain(matrix, part)
        testIdx = 1:floor(length(matrix)/part);
        trainIdx = (length(testIdx) + 1):length(matrix);
        
        test = matrix(testIdx,:);
        train = matrix(trainIdx,:);
        
end

function plotKMeansScatter(F, n)
    y = kmeans(F,n);
    x = 1:length(y);
    scatter(x,y);
end

function des = extract(filename)

[audio, SR] = audioread(filename);
audio = Descriptors.toMono(audio);
%The framsize and hopsize for (respectively) the attack, decay and
%energyband descriptors
frameSizes = [1024, 1024*32, 1024*4];
hopSizes = frameSizes/2;

des = Descriptors.extract(audio,SR, 8, frameSizes, hopSizes); 
%Descriptors.plotSpectrum(audio, SR, 1024*2, 512*2);

end

function [china,crash,hiHatClosed,hiHatOpen,kick,rideBell,rideBow,snare,tomHigh,tomLow] = readAndConcat(folder,idx)


[china,crash,hiHatClosed,hiHatOpen,kick,rideBell,rideBow,snare,tomHigh,tomLow] = readData(folder,idx(1));


for i = 2:length(idx)
    [china_,crash_,hiHatClosed_,hiHatOpen_,kick_,rideBell_,rideBow_,snare_,tomHigh_,tomLow_] = readData(folder,idx(i));
    china = [china;china_];
    crash = [crash;crash_];
    hiHatClosed = [hiHatClosed;hiHatClosed_];
    hiHatOpen = [hiHatOpen;hiHatOpen_];
    kick = [kick;kick_];
    rideBell = [rideBell;rideBell_];
    rideBow = [rideBow;rideBow_];
    snare = [snare;snare_];
    tomHigh = [tomHigh;tomHigh_];
    tomLow = [tomLow;tomLow_];
    
end


end

function [china,crash,hiHatClosed,hiHatOpen,kick,rideBell,rideBow,snare,tomHigh,tomLow] = readData(folder,idx)

path =  sprintf('data\\%s_%d\\',folder,idx);


chinaPath = sprintf('%schina.txt',path);
crashPath = sprintf('%scrash.txt',path);
hiHatClosedPath = sprintf('%shiHatClosed.txt',path);
hiHatOpenPath = sprintf('%shiHatOpen.txt',path);
kickPath = sprintf('%skick.txt',path);
rideBellPath = sprintf('%srideBell.txt',path);
rideBowPath = sprintf('%srideBow.txt',path);
snarePath = sprintf('%ssnare.txt',path);
tomHighPath = sprintf('%stomHigh.txt',path);
tomLowPath = sprintf('%stomLow.txt',path);


china = dlmread(chinaPath);
crash = dlmread(crashPath);
hiHatClosed = dlmread(hiHatClosedPath);
hiHatOpen = dlmread(hiHatOpenPath);
kick = dlmread(kickPath);
rideBell = dlmread(rideBellPath);
rideBow = dlmread(rideBowPath);
snare = dlmread(snarePath);
tomHigh = dlmread(tomHighPath);
tomLow = dlmread(tomLowPath);

end

function lv = lableVector(m,lable)
    [c,~] = size(m);
    lv = cell(c,1);

    for i = 1:c
      lv{i} = lable;
    end

end

function labels = createlabels(m, l)
    [~,n]  = size(m);
    
    
    labels = {};
    
    for i = 1:n
       labels = [labels;lableVector(m{i},l{i})];
    end
end

function [range,means] = normValues(matrix)
    [~,c] = size(matrix);
    range = zeros(c,1);
    means = zeros(c,1);
    
    for i = 1:c
        s1 = max(matrix(:,i));
        s2 = min(matrix(:,i));
        diff = s1 - s2;
        diff(diff == 0) = eps;
        m = mean(matrix(:,i));
        range(i) = diff;
        means(i) = m;
    end
        
end

function x = normalize(matrix, range, means)
    [~,c] = size(matrix);
    
    for i = 1:c
        matrix(:,i) = (matrix(:,i) - means(i)) / range(i);
    end
    
    x = matrix;
end

function [STDs,means] = normStdValues(matrix)
    [~,c] = size(matrix);
    STDs = zeros(c,1);
    means = zeros(c,1);
    
    for i = 1:c
        means(i) = mean(matrix(:,i));
        STDs(i) = std(matrix(:,i));

    end
        STDs(STDs == 0) = eps;
end


function x = normalizeStd(matrix, STDs, means)
    [~,c] = size(matrix);
    
    for i = 1:c
        matrix(:,i) = (matrix(:,i) - means(i)) / STDs(i);
    end
    
    x = matrix;
end

function [range,mins] = normMinValues(matrix)
    [~,c] = size(matrix);
    range = zeros(c,1);
    mins = zeros(c,1);
    
    for i = 1:c
        s1 = max(matrix(:,i));
        s2 = min(matrix(:,i));
        diff = s1 - s2;
        diff(diff == 0) = eps;

        range(i) = diff;
        mins(i) = s2;
    end
        
end

function x = normalizeMin(matrix, range, mins)
    [~,c] = size(matrix);
    
    for i = 1:c
        matrix(:,i) = (matrix(:,i) - mins(i)) / range(i);
    end
    
    x = matrix;
end

function [naNCount, infCount] = countNaN(X)
[r,c] = size(X);
naNCount = 0;
infCount = 0;
for i = 1:r
    for j = 1:c
        if(isnan(X(i,j)))
            naNCount = naNCount + 1;

        end
        if(X(i,j) == Inf)
            infCount = infCount + 1;
        end
    end
end
disp(naNCount);
disp(infCount);

end

function X = heal(matrix)

matrix(isnan(matrix)) = 0;
vector = matrix(:, 8);
vector(vector > 20) = 20;
matrix(:, 8) = vector;
X = matrix;



%{
[r,c] = size(matrix);
mCopy = matrix;

for i = 1:r
    for j = 1:c
        if(isnan(mCopy(i,j)))
            matrix(i,j) = 0;
        end
        
        if(mCopy(i,j) == Inf)
            matrix(i,j) = 0;
        end
    end
end
    
for i = 1:r
    for j = 1:c
        if(isnan(mCopy(i,j)))
            matrix(i,j) = mean(matrix(:,j));
        end
        
        if(mCopy(i,j) == Inf)
            matrix(i,j) = mean(matrix(:,j));
        end
    end
end   
X = matrix;

%}    


end

function m = healAndNormalize(matrix)
    matrix = heal(matrix);
    [range, means] = normValues(matrix);
    matrix = normalize(matrix, range, means);
    
    m = matrix;
end

function writeMatrixAndLabels(matrix,labels, name)
[~,c] = size(matrix);
m = 1:c;

nameFeatures = sprintf('%s.csv',name);
nameLabels = sprintf('%sLabels.csv',name);

dlmwrite(nameFeatures,m);
dlmwrite(nameFeatures,matrix,'-append');

fid=fopen(nameLabels,'wt');
fprintf(fid,'label\n');

[rows,cols]=size(labels);
for i=1:rows
      fprintf(fid,'%s,',labels{i,1:end-1});
      fprintf(fid,'%s\n',labels{i,end});
end
fclose(fid);


end
