library(FNN)





trainlabels <- read.csv("trainSetLabels.csv");
trainlabels <- as.factor(trainlabels[,]);

testlabels<-read.csv("testSetLabels.csv")
testlabels<-as.factor(testlabels[,])

testset<-read.csv("testSet.csv")
testset<-as.matrix(testset)

trainset<-read.csv("trainSet.csv")
trainset<-as.matrix(trainset)


knn(trainset, testset, trainLabels, k = 63, l = 0, prob = FALSE, use.all = TRUE)
#knn(trainset, testset, trainLabels, k = 63, l = 0, prob = FALSE, use.all = TRUE)