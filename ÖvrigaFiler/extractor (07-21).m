function extractor()
    

%First prototype
%{
   cymbal = descMatrix('D:\KTH\Exjobb\Avatar\Cymbals', 'Cymbals');
   tom = descMatrix('D:\KTH\Exjobb\Avatar\Floortom 1', 'Tom');
   hihat = descMatrix('D:\KTH\Exjobb\Avatar\Hihat\Closed Edge', 'HiHat');
   kick = descMatrix('D:\KTH\Exjobb\Avatar\Kick', 'Kick');
   ride = descMatrix('D:\KTH\Exjobb\Avatar\Ride 3', 'Ride');
   snare = descMatrix('D:\KTH\Exjobb\Avatar\Snare', 'Snare');
    dlmwrite('data\cymbal.txt',cymbal);
    dlmwrite('data\tom.txt',tom);
    dlmwrite('data\hihat.txt',hihat);
    dlmwrite('data\kick.txt',kick);
    dlmwrite('data\ride.txt',ride);
    dlmwrite('data\snare.txt',snare);
%}


tic
china = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\China.wav', 'China');
    dlmwrite('data_4\china.txt',china);

crash = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\Crash.wav', 'Crash');
    dlmwrite('data_4\crash.txt',crash);

hiHatClosed = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\HiHatClosed.wav', 'HiHatClosed');
    dlmwrite('data_4\hiHatClosed.txt',hiHatClosed);

hiHatOpen = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\HiHatOpen.wav', 'HiHatOpen');
    dlmwrite('data_4\hiHatOpen.txt',hiHatOpen);

kick = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\Kick.wav', 'Kick');
    dlmwrite('data_4\kick.txt',kick);

rideBell = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\RideBell.wav', 'RideBell');
    dlmwrite('data_4\rideBell.txt',rideBell);

rideBow = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\RideBow.wav', 'RideBow');
    dlmwrite('data_4\rideBow.txt',rideBow);

snare = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\Snare.wav', 'Snare');
    dlmwrite('data_4\snare.txt',snare);

tomHigh = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\TomHigh.wav', 'TomHigh');
    dlmwrite('data_4\tomHigh.txt',tomHigh);

tomLow = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\TomLow.wav', 'TomLow');
    dlmwrite('data_4\tomLow.txt',tomLow);

toc

end

function des = extract_old(filename)

[audio, SR] = audioread(filename);

%The framsize and hopsize for (respectively) the attack, decay and
%energyband descriptors
frameSizes = [1024, 1024*32, 1024*4];
hopSizes = frameSizes/2;

des = Descriptors.extract(audio,SR, 12, frameSizes, hopSizes); 
%Descriptors.plotSpectrum(audio, SR, 1024*2, 512*2);

end

function des = descMatrix_old(path, name)

list = ListAllFiles(path);
des = zeros(length(list),48);

for i = 1:length(list)
    str = sprintf('%s: Analysed %d of %d.',name,i,length(list));
    disp(str);
    des(i,:) = extract_old(list{i});
end

end

function descriptors = descMatrix(path, name)

[fullAudio, SR] = audioread(path);
fullAudio = Descriptors.toMono(fullAudio);
n = length(fullAudio)/(SR*8);
descriptors = zeros(n,48);

%The framsize and hopsize for (respectively) the attack, decay and
%energyband descriptors
% For data_1:
%frameSizes = [1024, 1024*32, 1024*4];
%hopSizes = frameSizes/2;

%For data_2
frameSizes = [512, 1024*32, 1024*4];
hopSizes = frameSizes/2;

for i = 1:n
    idxFrom = 1+(i-1)*(SR*8);
    idxTo = i*(SR*8);
    audio = fullAudio(idxFrom:idxTo);
    str = sprintf('%s: Analysed %d of %d.',name,i,n);
    disp(str);
    descriptors(i,:) = Descriptors.extract(audio,SR, 0, frameSizes, hopSizes); 
end

end

