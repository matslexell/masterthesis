classdef FileWriter
    methods (Static)
        
        function [labels, matrix] = readMatrixAndLabels()
        
        function [china,crash,hiHatClosed,hiHatOpen,kick,rideBell,rideBow,snare,tomHigh,tomLow] = readData(folder,idx)
            
            path =  sprintf('data\%s_%d\',folder,idx);
            
            
            chinaPath = sprintf('%schina.txt',path);
            crashPath = sprintf('%scrash.txt',path);
            hiHatClosedPath = sprintf('%shiHatClosed.txt',path);
            hiHatOpenPath = sprintf('%shiHatOpen.txt',path);
            kickPath = sprintf('%skick.txt',path);
            rideBellPath = sprintf('%srideBell.txt',path);
            rideBowPath = sprintf('%srideBow.txt',path);
            snarePath = sprintf('%ssnare.txt',path);
            tomHighPath = sprintf('%stomHigh.txt',path);
            tomLowPath = sprintf('%stomLow.txt',path);
            
            
            china = dlmread(chinaPath);
            crash = dlmread(crashPath);
            hiHatClosed = dlmread(hiHatClosedPath);
            hiHatOpen = dlmread(hiHatOpenPath);
            kick = dlmread(kickPath);
            rideBell = dlmread(rideBellPath);
            rideBow = dlmread(rideBowPath);
            snare = dlmread(snarePath);
            tomHigh = dlmread(tomHighPath);
            tomLow = dlmread(tomLowPath);
            
        end
    end
    
end