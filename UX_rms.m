% M-file 9.24
% UX_rms.m

clear; clf

%----- USER DATA -----
[DAFx_in, SR]  = wavread('flute2.wav');
hop            = 256;  % hop size between two FFTs
WindowLength   = 1024; % length of the windows
w              = hanningz(WindowLength);


%----- some initialisations -----
WindowLength2  = WindowLength/2;
normW          = norm(w,2);
pft            = 1;
lf             = floor((length(DAFx_in) - WindowLength)/hop);
feature_rms    = zeros(lf,1);


tic
%===========================================
pin  = 0;
pend = length(DAFx_in) - WindowLength;

while pin<pend
   % A grain is a windowed part of the input signal
   grain = DAFx_in(pin+1:pin+WindowLength).* w;
   feature_rms(pft) = norm(grain,2) / normW;
   pft   = pft + 1;
   
   
   pin   = pin + hop;
end
% ===========================================
toc


n_signal = 1:pend; 
t_signal = (n_signal-1)/SR; % time in sec

%pft is one longer than the size (and the size is one longer than required)
%hop/SR is the amount of seconds past per hop
t_rms = (0:pft-2)*hop/SR;

subplot(2,2,1); 
plot(t_signal, DAFx_in(n_signal)); 
title('Signal')
xlabel('t/s \rightarrow')
%axis([0 max(t_signal) [-1.2 1.2]*max(abs(DAFx_in))])
axis([0 max(t_signal) -1 1])

subplot(2,2,2); 
plot(t_rms, feature_rms);
title('RMS')
xlabel('t/s \rightarrow')
%axis([0 max(t_signal) 0 1.2*max(feature_rms)])
axis([0 max(t_signal) 0 1])



envelope = abs(hilbert(DAFx_in));
maxEnv = max(envelope);
attack_start = find(envelope >= maxEnv*0.2,1,'first');
attack_end = find(envelope >= maxEnv*0.8,1,'first');
disp(attack_start / SR);
disp(attack_end / SR);


maxEnv = max(feature_rms);
attack_start = find(feature_rms >= maxEnv*0.2,1,'first');
attack_end = find(feature_rms >= maxEnv*0.8,1,'first');
disp(length(DAFx_in) / SR);
disp(length(DAFx_in) / length(feature_rms));
disp(floor(attack_start * length(DAFx_in) / length(feature_rms)));
disp(floor(attack_end * length(DAFx_in) / length(feature_rms)));


subplot(2,2,3); 
plot(w);

val = norm(DAFx_in,2) / sqrt(length(DAFx_in));
val2 = sum(feature_rms) / length(feature_rms);
%disp(val);
%disp(val2);


