% M-file 9.24
% UX_rms.m

clear; clf

%----- USER DATA -----
[DAFx_in, SR]  = wavread('flute2.wav');
hop            = 256;  % hop size between two FFTs
WindowLength   = 1024; % length of the windows
w              = hanningz(WindowLength);

%----- some initialisations -----
WindowLength2  = WindowLength/2;
normW          = norm(w,2);
pft            = 1;
lf             = floor((length(DAFx_in) - WindowLength)/hop);
feature_rms    = zeros(lf,1);

ret_vector = [];

tic
%===========================================
pin  = 0;
pend = length(DAFx_in) - WindowLength;

while pin<pend
   grain = DAFx_in(pin+1:pin+WindowLength).* w;
   feature_rms(pft) = norm(grain,2) / normW;
   pft   = pft + 1;
   pin   = pin + hop;
end
% ===========================================
toc

n_signal = 1:pend; 
t_signal = (n_signal-1)/SR; % time in sec

t_rms = (0:pft-2)*hop/SR;

subplot(2,2,1); 
plot(t_signal, DAFx_in(n_signal)); 
title('Signal')
xlabel('t/s \rightarrow')
%axis([0 max(t_signal) [-1.2 1.2]*max(abs(DAFx_in))])
axis([0 max(t_signal) -1 1])

subplot(2,2,2); 
plot(t_rms, feature_rms);
title('RMS')
xlabel('t/s \rightarrow')
%axis([0 max(t_signal) 0 1.2*max(feature_rms)])
axis([0 max(t_signal) 0 1]);


%%MY EQUATIONS START HERE 
%%ATTACK SEGMENT
envelope = abs(hilbert(DAFx_in));
maxEnv = max(envelope);
attack_start = find(envelope >= maxEnv*0.2,1,'first');
attack_end = find(envelope >= maxEnv*0.8,1,'first');
plot(envelope);

%Time from - to of the attack phase
%disp(attack_start/SR);
%disp(attack_end/SR);
%disp(max(envelope));

attackLength = attack_end/SR - attack_start/SR;

% #1 ATTACK ENERGY

attackEnergy = 5;

% #2 TEMPORAL CENTROID

%REMOVE these lines: //TODO 
%attack_start = 2;
%attack_end = 8;
%feature_rms = [7,6,4,6,8,3,3,7,9,4];
%t_rms =   [1,2,3,4,5,6,7,8,9,10];
%To here

%Important here:
TC_rms = feature_rms(attack_start:attack_end);
TC_t = t_rms(attack_start:attack_end);
TC = sum(TC_rms.*TC_t) / sum(TC_rms);
%disp(TC);
ret_vector(2) = TC;
disp('hey')



% #3  LOG ATTACK-TIME
pow = 4^2;
LogAttackTime = log(attackLength);
disp(LogAttackTime);
ret_vector(3) = LogAttackTime;


% #4  ATTACK ZERO-CROSSING RATE

zerocrossing = 0;

for i = 2:length(DAFx_in)
   if(DAFx_in(i) * DAFx_in(i-1) < 0)
    zerocrossing = zerocrossing + 1;
   end
end

disp(zerocrossing);
ret_vector(4) = zerocrossing;

% #5  TC/EA

TC_EA = TC/attackLength;
disp(TC_EA);

ret_vector(5) = TC_EA;


% #6  DECAY SPECTRAL FLATNESS

% #7  DECAY SPECTRAL CENTROID


% #8  DECAY STRONG PEAK



% #9  DECAY SPECTRAL KURTOSIS


% #10 DECAY ZERO-CROSSING RATE


% #11 STRONG DECAY


% #12 DECAY SPECTRAL CENTROID VARIANCE


% #13 DECAY ZERO-CROSSING RATE VARIANCE


% #14 DECAY SKEWNESS



% ENERGY BANDS %
% #15 40-70 Hz

% #16 70-110 Hz

% #17 130-145 Hz

% #18 160-190 Hz

% #19 300-400 Hz

% #20 5-7 KHz

% #21 7-10 KHz

% #22 10-15 KHz


% Mel-Frequency Cepstrum Coefficients (mean values) %
% #23



% Mel-Frequency Cepstrum Coefficients (variances) %

ret_vector(48) = 0;
