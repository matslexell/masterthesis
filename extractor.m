function extractor()
    
%readAndWriteSubAudio('D:\KTH\Exjobb\SampleData\Trainingset\China.wav','fail4.wav',3528001:3880800);

%writeExampledata();

%{
readSingleFile('audioexamples\China.wav');
readSingleFile('audioexamples\Crash.wav');
readSingleFile('audioexamples\HiHatClosed.wav');
readSingleFile('audioexamples\HiHatOpen.wav');
readSingleFile('audioexamples\Kick.wav');
readSingleFile('audioexamples\RideBell.wav');
readSingleFile('audioexamples\RideBow.wav');
readSingleFile('audioexamples\Snare.wav');
readSingleFile('audioexamples\TomHigh.wav');
readSingleFile('audioexamples\TomLow.wav');


readSingleFile('audioexamples2\China.wav');
readSingleFile('audioexamples2\Crash.wav');
readSingleFile('audioexamples2\HiHatClosed.wav');
readSingleFile('audioexamples2\HiHatOpen.wav');
readSingleFile('audioexamples2\Kick.wav');
readSingleFile('audioexamples2\RideBell.wav');
readSingleFile('audioexamples2\RideBow.wav');
readSingleFile('audioexamples2\Snare.wav');
readSingleFile('audioexamples2\TomHigh.wav');
readSingleFile('audioexamples2\TomLow.wav');
%}


%readAndWriteTraindata();
%readAndWriteTestdata();


plotWave();
%savePlots(16384,8192,0);
%saveMfccplot();


end

function saveMfccplot()
path = {'China','Crash','HiHatClosed','HiHatOpen','Kick','RideBow','RideBell','Snare','TomHigh','TomLow'};
name = {'china cymbal','crash cymbal','hi-hat closed','hi-hat open','kick','ride bow','ride bell','snare','high tom','low tom'};

%path = {'China'};
%name = {'china cymbal'};



for i = 1:length(path)
    readpath =  sprintf('audioexamples\\%s.wav',path{i});
    [audio, SR] = audioread(readpath);
    % audio = audio(1:length(audio)/2);
    Descriptors.plotMfcc(audio, SR,name{i});
    writepath = sprintf('mfccplots\\%sMfcc',path{i});
    print(writepath,'-dpng');
end

for i = 1:length(path)
    readpath =  sprintf('audioexamples2\\%s.wav',path{i});
    [audio, SR] = audioread(readpath);
    % audio = audio(1:length(audio)/2);
    Descriptors.plotMfcc(audio, SR,name{i});
    writepath = sprintf('mfccplots\\2%sMfcc',path{i});
    print(writepath,'-dpng');
end

end

function plotWave()


[audio, SR] = audioread('audioexamples\Crash.wav');


    m = max(abs(audio));
    audio = audio / m;
    

plot(audio);

print('plots\audio','-dpng');

end

function savePlots(frameSize, hop, divide)
d = [4 4 0.7 1.1 1.8 4 4 1.7 2.7 4];
if(divide ~= 0)
   d = [divide divide divide divide divide divide divide divide divide divide];
end


plotData('audioexamples\Crash.wav','crash cymbal',frameSize, hop, d(2));
print('plots\crash','-dpng');

%{

plotData('audioexamples\China.wav','china cymbal',frameSize, hop, d(1));
print('plots\china','-dpng');

plotData('audioexamples\Crash.wav','crash cymbal',frameSize, hop, d(2));
print('plots\crash','-dpng');

plotData('audioexamples\HiHatClosed.wav','hi-hat closed',frameSize, hop,d(3));
print('plots\hiHatClosed','-dpng');

plotData('audioexamples\HiHatOpen.wav','hi-hat open',frameSize, hop,d(4));
print('plots\hiHatOpen','-dpng');

plotData('audioexamples\Kick.wav','kick',frameSize, hop,d(5));
print('plots\kick','-dpng');


plotData('audioexamples\RideBow.wav','ride bow',frameSize, hop,d(6));
print('plots\rideBow','-dpng');

plotData('audioexamples\RideBell.wav','ride bell',frameSize, hop,d(7));
print('plots\rideBell','-dpng');

plotData('audioexamples\Snare.wav','snare',frameSize, hop,d(8));
print('plots\snare','-dpng');

plotData('audioexamples\TomHigh.wav','high tom',frameSize, hop,d(9));
print('plots\tomHigh','-dpng');

plotData('audioexamples\TomLow.wav','low tom',frameSize, hop,d(10));
print('plots\tomLow','-dpng');





plotData('audioexamples2\China.wav','china cymbal',frameSize, hop,d(1));
print('plots\2china','-dpng');

plotData('audioexamples2\Crash.wav','crash cymbal',frameSize, hop,d(2));
print('plots\2crash','-dpng');

plotData('audioexamples2\HiHatClosed.wav','hi-hat closed',frameSize, hop,d(3));
print('plots\2hiHatClosed','-dpng');

plotData('audioexamples2\HiHatOpen.wav','hi-hat open',frameSize, hop,d(4));
print('plots\2hiHatOpen','-dpng');

plotData('audioexamples2\Kick.wav','kick',frameSize, hop,d(5));
print('plots\2kick','-dpng');

plotData('audioexamples2\RideBow.wav','ride bow',frameSize, hop,d(6));
print('plots\2rideBow','-dpng');

plotData('audioexamples2\RideBell.wav','ride bell',frameSize, hop,d(7));
print('plots\2rideBell','-dpng');

plotData('audioexamples2\Snare.wav','snare',frameSize, hop,d(8));
print('plots\2snare','-dpng');

plotData('audioexamples2\TomHigh.wav','high tom',frameSize, hop,d(9));
print('plots\2tomHigh','-dpng');

plotData('audioexamples2\TomLow.wav','low tom',frameSize, hop,d(10));
print('plots\2tomLow','-dpng');


%}

end

function plotData(path, string, frameSize, hop, len)

    [audio, SR] = audioread(path);
        
    Descriptors.plotSpectrum(audio, SR, frameSize, hop, string, len);
end

function writeExampledata()

n = 8*44100*10;
idx1 = 1+n:8*44100+n;
idx2 = 1+n*2:8*44100+n*2;

inpath = 'D:\KTH\Exjobb\SampleData\Trainingset\Snare.wav';
outpath = 'audioexamples2\Snare.wav';
readAndWriteSubAudio(inpath,outpath,idx1);


inpath = 'D:\KTH\Exjobb\SampleData\Trainingset\China.wav';
outpath = 'audioexamples2\China.wav';
readAndWriteSubAudio(inpath,outpath,idx1);

inpath = 'D:\KTH\Exjobb\SampleData\Trainingset\HiHatOpen.wav';
outpath = 'audioexamples2\HiHatOpen.wav';
readAndWriteSubAudio(inpath,outpath,idx1);

inpath = 'D:\KTH\Exjobb\SampleData\Trainingset\HiHatClosed.wav';
outpath = 'audioexamples2\HiHatClosed.wav';
readAndWriteSubAudio(inpath,outpath,idx1);

inpath = 'D:\KTH\Exjobb\SampleData\Trainingset\Crash.wav';
outpath = 'audioexamples2\Crash.wav';
readAndWriteSubAudio(inpath,outpath,idx1);

inpath = 'D:\KTH\Exjobb\SampleData\Trainingset\TomHigh.wav';
outpath = 'audioexamples2\TomHigh.wav';
readAndWriteSubAudio(inpath,outpath,idx1);

inpath = 'D:\KTH\Exjobb\SampleData\Trainingset\TomLow.wav';
outpath = 'audioexamples2\TomLow.wav';
readAndWriteSubAudio(inpath,outpath,idx1);

inpath = 'D:\KTH\Exjobb\SampleData\Trainingset\Kick.wav';
outpath = 'audioexamples2\Kick.wav';
readAndWriteSubAudio(inpath,outpath,idx1);



inpath = 'D:\KTH\Exjobb\SampleData\Trainingset\RideBell.wav';
outpath = 'audioexamples2\RideBell.wav';
readAndWriteSubAudio(inpath,outpath,idx2);

inpath = 'D:\KTH\Exjobb\SampleData\Trainingset\RideBow.wav';
outpath = 'audioexamples2\RideBow.wav';
readAndWriteSubAudio(inpath,outpath,idx2);

%}


end

function readAndWriteTraindata()

tic
china = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\China.wav', 'China',1);
dlmwrite('data\train\china.txt',china);

crash = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\Crash.wav', 'Crash',2);
dlmwrite('data\train\crash.txt',crash);

hiHatClosed = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\HiHatClosed.wav', 'HiHatClosed',3);
dlmwrite('data\train\hiHatClosed.txt',hiHatClosed);

hiHatOpen = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\HiHatOpen.wav', 'HiHatOpen',4);
dlmwrite('data\train\hiHatOpen.txt',hiHatOpen);

kick = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\Kick.wav', 'Kick',5);
dlmwrite('data\train\kick.txt',kick);

rideBell = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\RideBell.wav', 'RideBell',6);
dlmwrite('data\train\rideBell.txt',rideBell);

rideBow = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\RideBow.wav', 'RideBow',7);
dlmwrite('data\train\rideBow.txt',rideBow);

snare = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\Snare.wav', 'Snare',8);
dlmwrite('data\train\snare.txt',snare);

tomHigh = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\TomHigh.wav', 'TomHigh',9);
dlmwrite('data\train\tomHigh.txt',tomHigh);

tomLow = descMatrix('D:\KTH\Exjobb\SampleData\Trainingset\TomLow.wav', 'TomLow',10);
dlmwrite('data\train\tomLow.txt',tomLow);

toc


end

function readAndWriteElectrodata()
    
tic


crash = descMatrix('D:\KTH\Exjobb\SampleData\Electro\Crash.wav', 'Crash',2);
dlmwrite('data\electro\crash.txt',crash);

hiHatClosed = descMatrix('D:\KTH\Exjobb\SampleData\Electro\HiHatClosed.wav', 'HiHatClosed',3);
dlmwrite('data\electro\hiHatClosed.txt',hiHatClosed);

hiHatOpen = descMatrix('D:\KTH\Exjobb\SampleData\Electro\HiHatOpen.wav', 'HiHatOpen',4);
dlmwrite('data\electro\hiHatOpen.txt',hiHatOpen);

kick = descMatrix('D:\KTH\Exjobb\SampleData\Electro\Kick.wav', 'Kick',5);
dlmwrite('data\electro\kick.txt',kick);

snare = descMatrix('D:\KTH\Exjobb\SampleData\Electro\Snare.wav', 'Snare',8);
dlmwrite('data\electro\snare.txt',snare);

toc

end

function readAndWriteTestdata()

tic
china = descMatrix('D:\KTH\Exjobb\SampleData\Testset\China.wav', 'China',11);
dlmwrite('data\test\china.txt',china);

crash = descMatrix('D:\KTH\Exjobb\SampleData\Testset\Crash.wav', 'Crash',12);
dlmwrite('data\test\crash.txt',crash);

hiHatClosed = descMatrix('D:\KTH\Exjobb\SampleData\Testset\HiHatClosed.wav', 'HiHatClosed',13);
dlmwrite('data\test\hiHatClosed.txt',hiHatClosed);

hiHatOpen = descMatrix('D:\KTH\Exjobb\SampleData\Testset\HiHatOpen.wav', 'HiHatOpen',14);
dlmwrite('data\test\hiHatOpen.txt',hiHatOpen);

kick = descMatrix('D:\KTH\Exjobb\SampleData\Testset\Kick.wav', 'Kick',15);
dlmwrite('data\test\kick.txt',kick);

rideBell = descMatrix('D:\KTH\Exjobb\SampleData\Testset\RideBell.wav', 'RideBell',16);
dlmwrite('data\test\rideBell.txt',rideBell);

rideBow = descMatrix('D:\KTH\Exjobb\SampleData\Testset\RideBow.wav', 'RideBow',17);
dlmwrite('data\test\rideBow.txt',rideBow);

snare = descMatrix('D:\KTH\Exjobb\SampleData\Testset\Snare.wav', 'Snare',18);
dlmwrite('data\test\snare.txt',snare);

tomHigh = descMatrix('D:\KTH\Exjobb\SampleData\Testset\TomHigh.wav', 'TomHigh',19);
dlmwrite('data\test\tomHigh.txt',tomHigh);

tomLow = descMatrix('D:\KTH\Exjobb\SampleData\Testset\TomLow.wav', 'TomLow',20);
dlmwrite('data\test\tomLow.txt',tomLow);

toc

end

function readSingleFile(path)
frameSizes = [64, 1024*16, 1024*16];
hopSizes = [32,frameSizes(2)/2,frameSizes(3)/2];
[audio,SR] = audioread(path);
m = max(abs(audio));
audio2 = audio / m;
plot(audio2);

sound(audio,SR);
d = Descriptors.extract(audio,SR, 0, frameSizes, hopSizes, 0)';

d2 = Descriptors.extract(audio,SR, 0, frameSizes, hopSizes, 1)';

for i = 1:length(d)
    c(i) = d(i) / d2(i);
end

c

%str = sprintf('%d\t%d',des(2),des(5));
%disp(str)

%audiowrite('fail3.wav',audio,SR);

end

function readAndWriteSubAudio(inPath, outPath, idx)
[audio,SR] = audioread(inPath);
audio = Descriptors.toMono(audio);
audio = audio(idx);
audiowrite(outPath,audio,SR);

end

function descriptors = descMatrix(path, name, info)
dlmwrite('ErrorLog.txt',info,'-append');
[fullAudio, SR] = audioread(path);
fullAudio = Descriptors.toMono(fullAudio);
n = length(fullAudio)/(SR*8);
descriptors = zeros(n,48);

%The framsize and hopsize for (respectively) the attack, decay and
%energyband descriptors
frameSizes = [16, 1024*8, 1024*8];
hopSizes = [8,1024*4, 1024*4];

for i = 1:n
    idxFrom = 1+(i-1)*(SR*8);
    idxTo = i*(SR*8);
    audio = fullAudio(idxFrom:idxTo);
    str = sprintf('%s: Analysed %d of %d.',name,i,n);
    disp(str);
    descriptors(i,:) = Descriptors.extract(audio,SR, 0, frameSizes, hopSizes, 1); 
    d = descriptors(i,:);
    errorLog(idxFrom,idxTo,d);
end

end

function errorLog(idxFrom,idxTo,d)
    for j = 1:length(d)
        if(isnan(d(j)))
            write = [NaN idxFrom idxTo j];
            dlmwrite('ErrorLog.txt',write,'-append');
            str = sprintf('idx: %d:%d, NaN error desc: %d',idxFrom,idxTo,j);
            disp(str);
        end
        
        if(d(j) == Inf)
            write = [Inf idxFrom idxTo j];
            dlmwrite('ErrorLog.txt',write,'-append');
            str = sprintf('idx: %d:%d, Inf error desc: %d',idxFrom,idxTo,j);
            disp(str);
        end
    end
end

