function list = ListAllFiles(path)
d = dir(fullfile(path, '*.wav'));
%disp(path);
if(length(d) == 0)
    folder = dir(path);
    list = {};
    for i = 1:length(folder)
        if (strfind(folder(i).name, '.'))
            %donothing
        elseif(folder(i).isdir )
            l = ListAllFiles(strcat(path,'\',folder(i).name));
            list = vertcat(list,l);
        end
    end
else
    
    list = cell(length(d),1);
    
    for i = 1:length(d);
        list{i} = strcat(path,'\',d(i).name);
    end
end

end




